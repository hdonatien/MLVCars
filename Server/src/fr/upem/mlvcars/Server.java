package fr.upem.mlvcars;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.time.ZonedDateTime;
import java.util.UUID;

import fr.upem.mlvcars.controllers.rentables.BuyablesController;
import fr.upem.mlvcars.controllers.rentables.IBuyablesController;
import fr.upem.mlvcars.controllers.rentables.IRentablesController;
import fr.upem.mlvcars.controllers.rentables.RentablesController;
import fr.upem.mlvcars.controllers.rents.IRentsController;
import fr.upem.mlvcars.controllers.rents.RentsController;
import fr.upem.mlvcars.controllers.users.IUsersController;
import fr.upem.mlvcars.controllers.users.UsersController;
import fr.upem.mlvcars.rentables.RentableType;
import fr.upem.mlvcars.users.UserType;
import fr.upem.mlvcars.vehicles.Car;
import fr.upem.output.ColoredOutput;
import fr.upem.output.ColoredOutput.OutputColor;
import fr.upem.rate.Rate;
import fr.upem.rent.RentablesManager;
import fr.upem.rent.RentsManager;
import fr.upem.user.Authentication;
import fr.upem.user.User;
import fr.upem.user.User.Title;
import fr.upem.user.UsersManager;

public class Server {
	private static Registry registry;
	
	public static void main(String[] args) {
		try {
			String path = Server.class.getResource("/").toString();
			
			String codebase = path + "Shared.jar";
			String policy = path + "sec.policy";
			
			System.setProperty("java.rmi.server.codebase", codebase);
			System.setProperty("java.security.policy", policy);

			registry = LocateRegistry.createRegistry(8089);

			UsersManager<UserType> um = new UsersManager<>(-1);
			UUID student = um.add(new User<UserType>(Title.Mr, "Donatien", "De la Harzounie", UserType.Student), new Authentication("student", "mdp"), 1L);
			UUID teacher = um.add(new User<UserType>(Title.Mr, "Michel", "Bref-Passons", UserType.Teacher), new Authentication("teacher", "mdp"), 2L);
			um.add(new User<UserType>(Title.Mr, "Jean-Louis", "Connard", UserType.Outside), new Authentication("out", "mdp"), 3L);
			
			RentablesManager<RentableType> rm = new RentablesManager<>();
			rm.add(new Car("Renault", "Mégane III", 2009, "CS 702 PE", 5), student, 750);
			UUID vectra = rm.add(new Car("Opel", "Vectra Break", 2002, "LE MLZ 77", 5), teacher, 250);
			rm.changeAddingDate(vectra, ZonedDateTime.now().minusYears(4)); // HACK
			rm.comment(vectra, new Rate("trer beles voiture serieut", 10));
			rm.comment(vectra, new Rate("Mouais, bof.", 6));
			rm.rented(vectra); // HACK
			
			RentsManager<RentableType> rm2 = new RentsManager<>();
			rm2.register(rm);
			
			IUsersController users = new UsersController(um);
			registry.rebind("users", users);
			
			IRentsController rents = new RentsController(um, rm2, rm);
			registry.rebind("rents", rents);
			
			IRentablesController rentables = new RentablesController(rm, um);
			registry.rebind("rentables", rentables);
			
			IBuyablesController buyables = new BuyablesController(rm, um);
			registry.rebind("buyables", buyables);
		} catch (Exception e) {
			ColoredOutput.println("Trouble: " + e, OutputColor.Red);
		}
	}
}
