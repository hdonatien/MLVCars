package fr.upem.mlvcars.controllers.rents;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.time.Duration;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

import fr.upem.mlvbank.BankService;
import fr.upem.mlvbank.BankServiceServiceLocator;
import fr.upem.mlvbank.Transaction;
import fr.upem.mlvcars.rentables.RentableType;
import fr.upem.mlvcars.users.UserType;
import fr.upem.rate.Rate;
import fr.upem.rent.IRentablesManager;
import fr.upem.rent.IRentsManager;
import fr.upem.rent.RegularRent;
import fr.upem.rent.RentsManager.Renting;
import fr.upem.store.Entity;
import fr.upem.user.AuthenticationTicket;
import fr.upem.user.IUsersManager;
import fr.upem.user.User;

public class RentsController extends UnicastRemoteObject implements IRentsController {
	private static final long serialVersionUID = 5303572466783856852L;
	
	private final IUsersManager<UserType> users;
	private final IRentsManager<RentableType> rents;
	private final IRentablesManager<RentableType> rentables;
	
	public RentsController(IUsersManager<UserType> users, IRentsManager<RentableType> rents, IRentablesManager<RentableType> rentables) throws RemoteException {
		this.users = Objects.requireNonNull(users, "Invalid users.");
		this.rents = Objects.requireNonNull(rents, "Invalid rents.");
		this.rentables = Objects.requireNonNull(rentables, "Invalid rentables.");
	}

	@Override
	public List<Entity<Renting>> getRentsForUser(AuthenticationTicket ticket) throws IllegalStateException, RemoteException {
		Optional<UUID> user = users.checkSecurity(ticket);
		if (!user.isPresent()) {
			throw new IllegalStateException("Not permitted action.");
		}
		
		Entity<User<UserType>> userData = users.getAuthenticatedUser(ticket).get();
		if (userData.getElement().getType() == UserType.Outside) {
			throw new IllegalStateException("Not permitted action.");
		}
		
		return rents.getRentablesByUser(user.get());
	}

	@Override
	public RentResponse takeRent(AuthenticationTicket ticket, UUID rentable, ZonedDateTime start, ZonedDateTime end) throws IllegalStateException, RemoteException {
		Optional<UUID> user = users.checkSecurity(ticket);
		if (!user.isPresent()) {
			throw new IllegalStateException("Not permitted action.");
		}
		
		Entity<User<UserType>> userData = users.getAuthenticatedUser(ticket).get();
		if (userData.getElement().getType() == UserType.Outside) {
			throw new IllegalStateException("Not permitted action.");
		}
		
		try {
			long hourPrice = rentables.getHourPrice(rentable);
			Duration durationInSeconds = Duration.between(start, end);
			long durationPrice = (long) (durationInSeconds.getSeconds() * hourPrice / 3600.);

			BankService bank = new BankServiceServiceLocator().getBankService();
			long number = users.getUserBankAccount(user.get()).get();
			
			if (bank.transact(new Transaction(number, durationPrice, "EUR", 0)) == 0) {
				rents.add(new RegularRent(start, end), user.get(), rentable, durationPrice);
				return RentResponse.Success;
			} else {
				return RentResponse.ErrorPaymentFailed;
			}
		} catch (Exception e) {
			return RentResponse.ErrorInternal;
		}
	}

	@Override
	public CancelResponse cancelRent(AuthenticationTicket ticket, UUID rent) throws IllegalStateException, RemoteException {
		Optional<UUID> user = users.checkSecurity(ticket);
		if (!user.isPresent()) {
			throw new IllegalStateException("Not permitted action.");
		}
		
		Entity<User<UserType>> userData = users.getAuthenticatedUser(ticket).get();
		if (userData.getElement().getType() == UserType.Outside) {
			throw new IllegalStateException("Not permitted action.");
		}
		
		try {
			// TODO : ajouter une vérification de propriétaire
			
			rents.cancel(rent);
			
			return CancelResponse.Success;
		} catch (Exception e) {
			return CancelResponse.ErrorInternal;
		}
	}

	@Override
	public FinishResponse finishRent(AuthenticationTicket ticket, UUID rent) throws IllegalStateException, RemoteException {
		return finishRent(ticket, rent, null);
	}
	
	@Override
	public FinishResponse finishRent(AuthenticationTicket ticket, UUID rent, Rate rate) throws IllegalStateException, RemoteException {
		Optional<UUID> user = users.checkSecurity(ticket);
		if (!user.isPresent()) {
			throw new IllegalStateException("Not permitted action.");
		}
		
		Entity<User<UserType>> userData = users.getAuthenticatedUser(ticket).get();
		if (userData.getElement().getType() == UserType.Outside) {
			throw new IllegalStateException("Not permitted action.");
		}
		
		try {
			// TODO : ajouter une vérification de propriétaire
			
			rents.declareFinished(rent);

			if (rate != null) {
				UUID rentable = rents.getRentableForRent(rent).get();
				rentables.comment(rentable, rate);
			}
				
			return FinishResponse.Success;
		} catch (Exception e) {
			return FinishResponse.ErrorInternal;
		}
	}

	@Override
	public ExtensionResponse extendRent(AuthenticationTicket ticket, UUID rent, ZonedDateTime newEnd) throws IllegalStateException, RemoteException {
		Optional<UUID> user = users.checkSecurity(ticket);
		if (!user.isPresent()) {
			throw new IllegalStateException("Not permitted action.");
		}
		
		Entity<User<UserType>> userData = users.getAuthenticatedUser(ticket).get();
		if (userData.getElement().getType() == UserType.Outside) {
			throw new IllegalStateException("Not permitted action.");
		}
		
		try {
			// TODO : ajouter une vérification de propriétaire
			
			Optional<Entity<Renting>> rentData = rents.getRent(rent);
			
			if (!rentData.isPresent()) {
				return ExtensionResponse.ErrorInvalidRent;
			}
			
			Entity<Renting> renting = rentData.get();
			
			Duration oldDuration = Duration.between(renting.getElement().getRent().getStart(), renting.getElement().getRent().getEnd());
			Duration newDuration = Duration.between(renting.getElement().getRent().getEnd(), newEnd);
			long oldPrice = renting.getElement().getPrice();
			long newPrice = (long) (newDuration.getSeconds() / (float) oldDuration.getSeconds() * oldPrice);
			
			BankService bank = new BankServiceServiceLocator().getBankService();
			long number = users.getUserBankAccount(user.get()).get();
			
			if (bank.transact(new Transaction(number, newPrice, "EUR", 0)) == 0) {
				rents.extend(rent, newEnd, newPrice);
				return ExtensionResponse.Success;
			} else {
				return ExtensionResponse.ErrorPaymentFailed;
			}
		} catch (Exception e) {
			return ExtensionResponse.ErrorInternal;
		}
	}
}
