package fr.upem.mlvcars.controllers.rentables;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Currency;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

import fr.upem.mlvbank.BankAccountSnapShot;
import fr.upem.mlvbank.BankService;
import fr.upem.mlvbank.BankServiceServiceLocator;
import fr.upem.mlvbank.Transaction;
import fr.upem.mlvcars.rentables.RentableType;
import fr.upem.mlvcars.users.UserType;
import fr.upem.rent.IRentablesManager;
import fr.upem.rent.Rentable;
import fr.upem.store.Entity;
import fr.upem.user.AuthenticationTicket;
import fr.upem.user.IUsersManager;
import fr.upem.user.User;

public class BuyablesController extends UnicastRemoteObject implements IBuyablesController {
	private static final long serialVersionUID = 7655333424301380315L;
	
	private final IRentablesManager<RentableType> rentables;
	private final IUsersManager<UserType> users;
	
	public BuyablesController(IRentablesManager<RentableType> rentables, IUsersManager<UserType> users) throws RemoteException {
		this.rentables = Objects.requireNonNull(rentables, "Invalid rentables.");
		this.users = Objects.requireNonNull(users, "Invalid users.");
	}

	@Override
	public List<Buyable> getBuyableRentables(AuthenticationTicket ticket) throws RemoteException {
		Optional<UUID> user = users.checkSecurity(ticket);
		if (!user.isPresent()) {
			throw new IllegalStateException("Not permitted action.");
		}
		
		Entity<User<UserType>> userData = users.getAuthenticatedUser(ticket).get();
		if (userData.getElement().getType() != UserType.Outside) {
			throw new IllegalStateException("Not permitted action.");
		}
		
		List<Entity<Rentable<RentableType>>> buyableRentables = rentables.getBuyables();
		List<Buyable> buyableRentablesData = new LinkedList<>();
		
		try {
			for (Entity<Rentable<RentableType>> e : buyableRentables) {
				long universalPrice = rentables.getHourPrice(e.getId());
				BankService bank = new BankServiceServiceLocator().getBankService();
				BankAccountSnapShot snap = bank.getSnapShot(users.getUserBankAccount(user.get()).get());
				long truePrice = bank.convert("EUR", snap.getCurrency(), universalPrice);
				
				buyableRentablesData.add(new RentableData(e, truePrice, Currency.getInstance(snap.getCurrency())).toBuyable());
			}
			
			return buyableRentablesData;
		} catch (Exception e) {
			return new LinkedList<Buyable>();
		}
	}

	@Override
	public BuyingResponse buy(AuthenticationTicket ticket, UUID buyable) throws IllegalArgumentException, RemoteException {
		Optional<UUID> user = users.checkSecurity(ticket);
		if (!user.isPresent()) {
			throw new IllegalStateException("Not permitted action.");
		}
		
		Entity<User<UserType>> userData = users.getAuthenticatedUser(ticket).get();
		if (userData.getElement().getType() != UserType.Outside) {
			throw new IllegalStateException("Not permitted action.");
		}
		
		try {
			if (!rentables.isBuyable(buyable)) {
				return BuyingResponse.ErrorRentableNotBuyable;
			}
			
			long account = users.getUserBankAccount(user.get()).get();
			long price = rentables.getBuyingPrice(buyable);
			
			BankService bank = new BankServiceServiceLocator().getBankService();
			
			if (bank.transact(new Transaction(account, price, "EUR", 0)) != 0) {
				return BuyingResponse.ErrorPaymentFailed;
			}

			rentables.remove(buyable);
			
			return BuyingResponse.Success;
		} catch (Exception e) {
			return BuyingResponse.ErrorInternal;
		}
	}

	@Override
	public Buyable getBuyable(AuthenticationTicket ticket, UUID buyable) throws IllegalArgumentException, RemoteException {
		List<Buyable> buyables = getBuyableRentables(ticket);
		Optional<Buyable> rentable = buyables.stream().filter(b -> b.getId().equals(buyable.toString())).findFirst();
		
		if (!rentable.isPresent()) {
			throw new IllegalArgumentException("Unknown buyable.");
		}
		
		return rentable.get();
	}
}
