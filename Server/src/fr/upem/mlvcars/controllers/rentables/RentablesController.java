package fr.upem.mlvcars.controllers.rentables;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Currency;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import fr.upem.mlvbank.BankAccountSnapShot;
import fr.upem.mlvbank.BankService;
import fr.upem.mlvbank.BankServiceServiceLocator;
import fr.upem.mlvcars.rentables.RentableType;
import fr.upem.mlvcars.users.UserType;
import fr.upem.mlvcars.vehicles.Car;
import fr.upem.rate.Rate;
import fr.upem.rent.IRentablesManager;
import fr.upem.rent.Rentable;
import fr.upem.store.Entity;
import fr.upem.user.AuthenticationTicket;
import fr.upem.user.IUsersManager;
import fr.upem.user.User;

public class RentablesController extends UnicastRemoteObject implements IRentablesController {
	private static final long serialVersionUID = -1551022962858138341L;
	
	private final IRentablesManager<RentableType> rentables;
	private final IUsersManager<UserType> users;
	
	public RentablesController(IRentablesManager<RentableType> rentables, IUsersManager<UserType> users) throws RemoteException {
		this.rentables = Objects.requireNonNull(rentables, "Invalid rentables.");
		this.users = Objects.requireNonNull(users, "Invalid users.");
	}

	@Override
	public List<RentableData> getRentablesForUser(AuthenticationTicket ticket) throws IllegalStateException, RemoteException {
		Optional<UUID> user = users.checkSecurity(ticket);
		if (!user.isPresent()) {
			throw new IllegalStateException("Not permitted action.");
		}
		
		Entity<User<UserType>> userData = users.getAuthenticatedUser(ticket).get();
		if (userData.getElement().getType() == UserType.Outside) {
			throw new IllegalStateException("Not permitted action.");
		}
		
		List<Entity<Rentable<RentableType>>> userRentables = rentables.getRentablesByOwner(user.get());
		List<RentableData> userRentablesData = new LinkedList<>();
		
		try {
			for (Entity<Rentable<RentableType>> e : userRentables) {
				long universalPrice = rentables.getHourPrice(e.getId());
				BankService bank = new BankServiceServiceLocator().getBankService();
				BankAccountSnapShot snap = bank.getSnapShot(users.getUserBankAccount(user.get()).get());
				long truePrice = bank.convert("EUR", snap.getCurrency(), universalPrice);
				
				userRentablesData.add(new RentableData(e, truePrice, Currency.getInstance(snap.getCurrency())));
			}
			
			return userRentablesData;
		} catch (Exception e) {
			return new LinkedList<RentableData>();
		}
	}

	@Override
	public List<RentableData> getRentables(AuthenticationTicket ticket) throws IllegalStateException, RemoteException {
		return getFilteredRentables(ticket, null);
	}

	@Override
	public List<RentableData> getFilteredRentables(AuthenticationTicket ticket, RentableType type) throws IllegalStateException, RemoteException {
		Optional<UUID> user = users.checkSecurity(ticket);
		if (!user.isPresent()) {
			throw new IllegalStateException("Not permitted action.");
		}
		
		Entity<User<UserType>> userData = users.getAuthenticatedUser(ticket).get();
		if (userData.getElement().getType() == UserType.Outside) {
			throw new IllegalStateException("Not permitted action.");
		}
		
		List<Entity<Rentable<RentableType>>> allRentables = rentables.getRentables();
		List<RentableData> allRentablesData = new LinkedList<>();
		
		try {
			for (Entity<Rentable<RentableType>> e : allRentables) {
				long universalPrice = rentables.getHourPrice(e.getId());
				BankService bank = new BankServiceServiceLocator().getBankService();
				BankAccountSnapShot snap = bank.getSnapShot(users.getUserBankAccount(user.get()).get());
				long truePrice = bank.convert("EUR", snap.getCurrency(), universalPrice);
				
				allRentablesData.add(new RentableData(e, truePrice, Currency.getInstance(snap.getCurrency())));
			}
			
			return allRentablesData.stream().filter(r -> type != null ? r.getRentable().getElement().getType() == type : true).collect(Collectors.toList());
		} catch (Exception e) {
			return new LinkedList<RentableData>();
		}
	}

	@Override
	public void createCar(AuthenticationTicket ticket, String constructor, String model, int year, String matriculation, int doors, long price) throws IllegalStateException, RemoteException {
		Optional<UUID> user = users.checkSecurity(ticket);
		if (!user.isPresent()) {
			throw new IllegalStateException("Not permitted action.");
		}
		
		Entity<User<UserType>> userData = users.getAuthenticatedUser(ticket).get();
		if (userData.getElement().getType() == UserType.Outside) {
			throw new IllegalStateException("Not permitted action.");
		}
		
		rentables.add(new Car(constructor, model, year, matriculation, doors), user.get(), price);
	}
	
	@Override
	public void modifyCar(AuthenticationTicket ticket, UUID car, String constructor, String model, int year, String matriculation, int doors, long price) throws IllegalStateException, RemoteException {
		Optional<UUID> user = users.checkSecurity(ticket);
		if (!user.isPresent()) {
			throw new IllegalStateException("Not permitted action.");
		}
		
		Entity<User<UserType>> userData = users.getAuthenticatedUser(ticket).get();
		if (userData.getElement().getType() == UserType.Outside) {
			throw new IllegalStateException("Not permitted action.");
		}
		
		if (!rentables.isOwner(user.get(), car)) {
			throw new IllegalStateException("Not permitted action.");
		}
		
		rentables.modify(car, new Car(constructor, model, year, matriculation, doors), price);
	}

	@Override
	public List<Entity<Rate>> getComments(AuthenticationTicket ticket, UUID rentable) throws IllegalStateException, RemoteException {
		Optional<UUID> user = users.checkSecurity(ticket);
		if (!user.isPresent()) {
			throw new IllegalStateException("Not permitted action.");
		}
		
		return rentables.getComments(rentable);
	}
}
