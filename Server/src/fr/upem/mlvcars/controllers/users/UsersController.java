package fr.upem.mlvcars.controllers.users;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

import fr.upem.mlvcars.users.UserType;
import fr.upem.store.Entity;
import fr.upem.user.Authentication;
import fr.upem.user.AuthenticationTicket;
import fr.upem.user.IUsersManager;
import fr.upem.user.User;

public class UsersController extends UnicastRemoteObject implements IUsersController {
	private static final long serialVersionUID = 4491339465961453716L;

	private final IUsersManager<UserType> users;

	public UsersController(IUsersManager<UserType> users) throws RemoteException {
		this.users = Objects.requireNonNull(users, "Invalid users manager.");
	}

	@Override
	public ConnectionResponse connect(String login, String password) throws RemoteException {
		Optional<AuthenticationTicket> ticket = users.connect(new Authentication(login, password));

		if (ticket.isPresent()) {
			AuthenticationTicket authTicket = ticket.get();
			Entity<User<UserType>> user = users.getAuthenticatedUser(authTicket).get();

			return new ConnectionResponse(authTicket, user);
		}

		return new ConnectionResponse(ConnectionResponse.Code.ErrorInvalidLoginAndOrPassword);
	}

	@Override
	public void disconnect(AuthenticationTicket ticket) throws RemoteException {
		users.disconnect(ticket);
	}

	@Override
	public long getUserBankAccount(AuthenticationTicket ticket) throws IllegalStateException, RemoteException {
		Optional<UUID> user = users.checkSecurity(ticket);
		if (!user.isPresent()) {
			throw new IllegalStateException("Not permitted action.");
		}
		
		return users.getUserBankAccount(user.get()).get();
	}
}
