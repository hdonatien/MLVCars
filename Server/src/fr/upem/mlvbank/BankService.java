/**
 * BankService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package fr.upem.mlvbank;

public interface BankService extends java.rmi.Remote {
    public long convert(java.lang.String currencyFrom, java.lang.String currencyTo, long amount) throws java.rmi.RemoteException;
    public fr.upem.mlvbank.BankAccountSnapShot getSnapShot(long number) throws java.rmi.RemoteException;
    public int transact(fr.upem.mlvbank.Transaction transaction) throws java.rmi.RemoteException;
}
