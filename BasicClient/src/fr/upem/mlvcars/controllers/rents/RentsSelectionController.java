package fr.upem.mlvcars.controllers.rents;

import java.util.List;
import java.util.Objects;
import java.util.Scanner;

import fr.upem.mlvcars.controllers.rentables.IRentablesController;
import fr.upem.mlvcars.controllers.users.ConnectionResponse;
import fr.upem.mlvcars.menus.InputController;
import fr.upem.mlvcars.menus.Menu;
import fr.upem.mlvcars.menus.TitledMenu;
import fr.upem.rent.RentsManager.Renting;
import fr.upem.store.Entity;

public class RentsSelectionController implements InputController<Entity<Renting>> {
	private final Menu<Entity<Renting>> rentSelector = new TitledMenu<>("=== Séléctionner une location ===");
	
	public RentsSelectionController(ConnectionResponse connection, IRentablesController rentables, List<Entity<Renting>> userRents) {
		Objects.requireNonNull(connection, "Invalid connection.");
		Objects.requireNonNull(rentables, "Invalid rentables.");
		Objects.requireNonNull(userRents, "Invalid user rentables.");
		
		for (Entity<Renting> renting : userRents) {
			rentSelector.addEntry(renting.getElement().getRent() + " -> " + renting.getElement().getRentable() + " [" + (renting.getElement().getPrice() / 100.) + "EUR/heure]", renting);
		}

		rentSelector.addEntry("Annuler.", null);
	}

	@Override
	public Entity<Renting> show(Scanner input) {
		return rentSelector.show(input);
	}
}
