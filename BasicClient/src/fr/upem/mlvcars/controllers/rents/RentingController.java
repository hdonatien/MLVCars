package fr.upem.mlvcars.controllers.rents;

import java.rmi.RemoteException;
import java.time.Duration;
import java.time.ZonedDateTime;
import java.util.Objects;
import java.util.Optional;
import java.util.Scanner;

import fr.upem.mlvcars.controllers.rentables.RentableData;
import fr.upem.mlvcars.controllers.users.ConnectionResponse;
import fr.upem.mlvcars.menus.BasicMenus;
import fr.upem.mlvcars.menus.BasicMenus.ConfirmResult;
import fr.upem.mlvcars.menus.InputTools;
import fr.upem.mlvcars.menus.MenuController;
import fr.upem.output.ColoredOutput;
import fr.upem.output.ColoredOutput.OutputColor;

public class RentingController implements MenuController {
	private final ConnectionResponse connection;
	
	private final IRentsController rents;
	
	private final RentableData rentable;
	
	public RentingController(ConnectionResponse connection, IRentsController rents, RentableData rentable) {
		this.connection = Objects.requireNonNull(connection, "Invalid connection.");
		this.rents = Objects.requireNonNull(rents, "Invalid rents.");
		this.rentable = Objects.requireNonNull(rentable, "Invalid rentable.");
	}

	@Override
	public void start(Scanner input) {
		ColoredOutput.println("=== Séléctionner une date de début de location ===");
		Optional<ZonedDateTime> beginDate = InputTools.getDate(input);
		ColoredOutput.println();
		
		ColoredOutput.println("=== Séléctionner une date de fin de location ===");
		Optional<ZonedDateTime> endDate = InputTools.getDate(input);
		ColoredOutput.println();
		
		if (beginDate.isPresent() && endDate.isPresent()) {
			ZonedDateTime beginDateTime = beginDate.get();
			ZonedDateTime endDateTime = endDate.get();
			
			if (endDateTime.isBefore(beginDateTime)) {
				ColoredOutput.println("La date de début doit être avant la date de fin. Abandon.\n", OutputColor.Red);
				return;
			}
			
			if (beginDateTime.isBefore(ZonedDateTime.now())) {
				ColoredOutput.println("La date saisie doit être dans le futur. Abandon.\n", OutputColor.Red);
				return;
			}
			
			try {
				long hourPrice = rentable.getPrice();
				Duration durationInSeconds = Duration.between(beginDateTime, endDateTime);
				long durationPrice = (long) (durationInSeconds.getSeconds() * hourPrice / 3600.);
				
				ColoredOutput.println("Prix de la location : " + (durationPrice / 100.) + rentable.getCurrency().getCurrencyCode() + "\n");
				
				ConfirmResult confirm = BasicMenus.confirm.show(input);
				
				if (confirm == ConfirmResult.No) {
					ColoredOutput.println("Location annulée.\n", OutputColor.Red);
					return;
				}
				
				RentResponse response = rents.takeRent(connection.getTicket(), rentable.getRentable().getId(), beginDateTime, endDateTime);
				
				if (response == RentResponse.Success) {
					ColoredOutput.println("Location effectuée.\n", OutputColor.Green);
				} else {
					ColoredOutput.println("La location a échoué, il se peut que le louable n'est pas disponible ou que vous n'ayez pas les fonds nécessaires.\n", OutputColor.Red);
				}
			} catch (IllegalStateException e) {
				ColoredOutput.println("Vous n'êtes pas connecté.\n", OutputColor.Red);
			} catch (RemoteException e) {
				ColoredOutput.println("Erreur de connexion.\n", OutputColor.Red);
			}
		} else {
			ColoredOutput.println("Les dates saisies sont invalides. Abandon.\n", OutputColor.Red);
		}
	}
}
