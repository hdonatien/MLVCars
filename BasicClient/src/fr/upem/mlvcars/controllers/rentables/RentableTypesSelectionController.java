package fr.upem.mlvcars.controllers.rentables;

import java.util.Scanner;

import fr.upem.mlvcars.menus.InputController;
import fr.upem.mlvcars.menus.Menu;
import fr.upem.mlvcars.menus.TitledMenu;
import fr.upem.mlvcars.rentables.RentableType;

public class RentableTypesSelectionController implements InputController<RentableType> {
	private final Menu<RentableType> typeSelector = new TitledMenu<>("=== Sélectionner un type de louable ===");
	
	public RentableTypesSelectionController() {
		for (RentableType type : RentableType.values()) {
			typeSelector.addEntry(type.getTitle(), type);
		}

		typeSelector.addEntry("Annuler.", null);
	}

	@Override
	public RentableType show(Scanner input) {
		return typeSelector.show(input);
	}
}
