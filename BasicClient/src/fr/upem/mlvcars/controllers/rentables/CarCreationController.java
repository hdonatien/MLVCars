package fr.upem.mlvcars.controllers.rentables;

import java.rmi.RemoteException;
import java.util.Objects;
import java.util.Optional;
import java.util.Scanner;

import fr.upem.mlvcars.controllers.rentables.IRentablesController;
import fr.upem.mlvcars.controllers.users.ConnectionResponse;
import fr.upem.mlvcars.menus.InputTools;
import fr.upem.mlvcars.menus.MenuController;
import fr.upem.output.ColoredOutput;
import fr.upem.output.ColoredOutput.OutputColor;

public class CarCreationController implements MenuController {
	private final ConnectionResponse connection;

	private final IRentablesController rentables;

	public CarCreationController(ConnectionResponse connection, IRentablesController rentables) {
		this.connection = Objects.requireNonNull(connection, "Invalid connection.");
		this.rentables = Objects.requireNonNull(rentables, "Invalid rentables.");
	}

	@Override
	public void start(Scanner input) {
		ColoredOutput.print("Constructeur : ");
		Optional<String> constructor = InputTools.getString(input);

		ColoredOutput.print("Modèle : ");
		Optional<String> model = InputTools.getString(input);

		ColoredOutput.print("Année : ");
		Optional<Integer> year = InputTools.getInt(input);

		ColoredOutput.print("Immatriculation : ");
		Optional<String> matriculation = InputTools.getString(input);

		ColoredOutput.print("Nombre de portes : ");
		Optional<Integer> doors = InputTools.getInt(input);

		ColoredOutput.print("Prix : ");
		Optional<Long> price = InputTools.getLong(input);

		if (constructor.isPresent() && model.isPresent() && year.isPresent() && matriculation.isPresent() && doors.isPresent() && price.isPresent()) {
			try {
				rentables.createCar(connection.getTicket(), constructor.get(), model.get(), year.get(), matriculation.get(), doors.get(), price.get());
				ColoredOutput.println("Création réussie.", OutputColor.Green);
				ColoredOutput.println();
			} catch (IllegalStateException e) {
				ColoredOutput.println("Vous n'êtes pas connecté.\n", OutputColor.Red);
			} catch (RemoteException e) {
				ColoredOutput.println("Erreur de connexion.\n", OutputColor.Red);
			}
		} else {
			ColoredOutput.println("Les informations saisies sont invalides. Abandon.\n", OutputColor.Red);
		}
	}
}
