package fr.upem.mlvcars.controllers.rentables;

import java.util.List;
import java.util.Objects;
import java.util.Scanner;

import fr.upem.mlvcars.controllers.users.ConnectionResponse;
import fr.upem.mlvcars.menus.InputController;
import fr.upem.mlvcars.menus.Menu;
import fr.upem.mlvcars.menus.TitledMenu;

public class RentablesSelectionController implements InputController<RentableData> {
	private final Menu<RentableData> rentableSelector = new TitledMenu<>("=== Séléctionner un louable ===");
	
	public RentablesSelectionController(ConnectionResponse connection, IRentablesController rentables, List<RentableData> userRentables) {
		Objects.requireNonNull(connection, "Invalid connection.");
		Objects.requireNonNull(rentables, "Invalid rentables.");
		Objects.requireNonNull(userRentables, "Invalid user rentables.");
		
		for (RentableData rentable : userRentables) {
			rentableSelector.addEntry(rentable.getRentable().getElement().toString() + " [" + (rentable.getPrice() / 100.) + rentable.getCurrency().getCurrencyCode() + "/heure]", rentable);
		}

		rentableSelector.addEntry("Annuler.", null);
	}

	@Override
	public RentableData show(Scanner input) {
		return rentableSelector.show(input);
	}
}
