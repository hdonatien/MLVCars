package fr.upem.mlvcars.controllers.rentables;

import java.rmi.RemoteException;
import java.util.Objects;
import java.util.Optional;
import java.util.Scanner;
import java.util.UUID;

import fr.upem.mlvcars.controllers.rentables.IRentablesController;
import fr.upem.mlvcars.controllers.rentables.RentableData;
import fr.upem.mlvcars.controllers.users.ConnectionResponse;
import fr.upem.mlvcars.menus.InputTools;
import fr.upem.mlvcars.menus.MenuController;
import fr.upem.mlvcars.vehicles.Car;
import fr.upem.output.ColoredOutput;
import fr.upem.output.ColoredOutput.OutputColor;

public class CarModificationController implements MenuController {
	private final ConnectionResponse connection;

	private final IRentablesController rentables;
	
	private final RentableData rentable;
	
	public CarModificationController(ConnectionResponse connection, IRentablesController rentables, RentableData rentable) {
		this.connection = Objects.requireNonNull(connection, "Invalid connection.");
		this.rentables = Objects.requireNonNull(rentables, "Invalid rentables.");
		this.rentable = Objects.requireNonNull(rentable, "Invalid rentable.");
	}
	
	@Override
	public void start(Scanner input) {
		Car car = (Car) rentable.getRentable().getElement();
		UUID carId = rentable.getRentable().getId();

		ColoredOutput.print("Constructeur (" + car.getConstructor() + ") : ");
		Optional<String> constructor = InputTools.getString(input);

		ColoredOutput.print("Modèle (" + car.getModel() + ") : ");
		Optional<String> model = InputTools.getString(input);

		ColoredOutput.print("Année (" + car.getYear() + ") : ");
		Optional<Integer> year = InputTools.getInt(input);

		ColoredOutput.print("Immatriculation (" + car.getMatriculation() + ") : ");
		Optional<String> matriculation = InputTools.getString(input);

		ColoredOutput.print("Nombre de portes (" + car.getDoors() + ") : ");
		Optional<Integer> doors = InputTools.getInt(input);

		ColoredOutput.print("Prix (" + rentable.getPrice() + ") : ");
		Optional<Long> price = InputTools.getLong(input);
		
		Car newCar = new Car(
			constructor.isPresent() && constructor.get().length() > 0 ? constructor.get() : car.getConstructor(),
			model.isPresent() && model.get().length() > 0 ? model.get() : car.getModel(),
			year.isPresent() ? year.get() : car.getYear(),
			matriculation.isPresent() && matriculation.get().length() > 0 ? matriculation.get() : car.getMatriculation(),
			doors.isPresent() ? doors.get() : car.getDoors()
		);
		
		Long newPrice = price.isPresent() ? price.get() : rentable.getPrice();

		if (car.equals(newCar) && newPrice.equals(rentable.getPrice())) {
			ColoredOutput.println("Voiture non modifiée.\n");
			return;
		}

		try {
			rentables.modifyCar(connection.getTicket(), carId, newCar.getConstructor(), newCar.getModel(), newCar.getYear(), newCar.getMatriculation(), newCar.getDoors(), newPrice);
			ColoredOutput.println("Voiture modifiée.\n", OutputColor.Green);
		} catch (IllegalStateException e) {
			ColoredOutput.println("Vous n'êtes pas connecté.\n", OutputColor.Red);
		} catch (RemoteException e) {
			ColoredOutput.println("Erreur de connexion.\n", OutputColor.Red);
		}
	}
}
