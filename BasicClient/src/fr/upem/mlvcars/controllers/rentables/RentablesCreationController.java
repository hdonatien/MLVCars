package fr.upem.mlvcars.controllers.rentables;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Scanner;

import fr.upem.mlvcars.controllers.users.ConnectionResponse;
import fr.upem.mlvcars.menus.MenuController;
import fr.upem.mlvcars.rentables.RentableType;
import fr.upem.output.ColoredOutput;
import fr.upem.output.ColoredOutput.OutputColor;

public class RentablesCreationController implements MenuController {
	private final RentableTypesSelectionController typeSelector = new RentableTypesSelectionController();
	private final Map<RentableType, MenuController> creators = new HashMap<>();

	public RentablesCreationController(ConnectionResponse connection, IRentablesController rentables) {
		Objects.requireNonNull(connection, "Invalid connection.");
		Objects.requireNonNull(rentables, "Invalid rentables.");

		creators.put(RentableType.Car, new CarCreationController(connection, rentables));
	}

	@Override
	public void start(Scanner input) {
		RentableType type = typeSelector.show(input);

		if (type != null) {
			MenuController creator = creators.get(type);
			
			if (creator == null) {
				ColoredOutput.println("Impossible de créer un louable de type " + type.getTitle() + "\n", OutputColor.Red);
			} else {
				creator.start(input);
				return;
			}
		}
	}
}
