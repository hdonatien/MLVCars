package fr.upem.mlvcars.controllers.mainmenu;

import java.util.Objects;
import java.util.Scanner;

import fr.upem.mlvcars.controllers.account.MyAccountController;
import fr.upem.mlvcars.controllers.find.FindController;
import fr.upem.mlvcars.controllers.rentables.IRentablesController;
import fr.upem.mlvcars.controllers.rents.IRentsController;
import fr.upem.mlvcars.controllers.users.ConnectionResponse;
import fr.upem.mlvcars.controllers.users.IUsersController;
import fr.upem.mlvcars.menus.Menu;
import fr.upem.mlvcars.menus.MenuController;
import fr.upem.mlvcars.menus.TitledMenu;

public class MainController implements MenuController {
	public static enum MainMenuEntry {
		MyAccount, Find, Quit
	}
	
	private ConnectionResponse connection;
	
	private final IUsersController users;
	private final IRentsController rents;
	private final IRentablesController rentables;
	
	private final Menu<MainMenuEntry> menu = new TitledMenu<>("=== Menu principal ===");
	
	public MainController(ConnectionResponse connection, IUsersController users, IRentsController rents, IRentablesController rentables) {
		this.connection = Objects.requireNonNull(connection, "Invalid connection.");
		this.users = Objects.requireNonNull(users, "Invalid users.");
		this.rents = Objects.requireNonNull(rents, "Invalid rents.");
		this.rentables = Objects.requireNonNull(rentables, "Invalid rentables.");
		
		menu.addEntry("Mon compte.", MainMenuEntry.MyAccount)
			.addEntry("Effectuer une recherche.", MainMenuEntry.Find)
			.addEntry("Quitter.", MainMenuEntry.Quit);
	}
	
	@Override
	public void start(Scanner input) {
		MainMenuEntry choice = null;
		
		do {
			choice = menu.show(input);
			
			switch (choice) {
			case MyAccount :
				if (myAccount(input) == false) { return; }
				break;
			case Find :
				find(input);
				break;
			case Quit :
				return;
			}
		} while (choice != MainMenuEntry.Quit);
	}
	
	private boolean myAccount(Scanner input) {
		MyAccountController myAccount = new MyAccountController(connection, users, rents, rentables);
		return myAccount.show(input);
	}
	
	private void find(Scanner input) {
		FindController finder = new FindController(connection, rents, rentables);
		finder.start(input);
	}
}
