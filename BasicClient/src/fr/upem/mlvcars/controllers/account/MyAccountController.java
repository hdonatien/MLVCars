package fr.upem.mlvcars.controllers.account;

import java.rmi.RemoteException;
import java.util.Objects;
import java.util.Scanner;

import fr.upem.mlvcars.controllers.rentables.IRentablesController;
import fr.upem.mlvcars.controllers.rents.IRentsController;
import fr.upem.mlvcars.controllers.users.ConnectionResponse;
import fr.upem.mlvcars.controllers.users.IUsersController;
import fr.upem.mlvcars.menus.InputController;
import fr.upem.mlvcars.menus.Menu;
import fr.upem.mlvcars.menus.TitledMenu;
import fr.upem.output.ColoredOutput;
import fr.upem.output.ColoredOutput.OutputColor;
import fr.upem.user.User.Title;

public class MyAccountController implements InputController<Boolean> {
	public static enum MyAccountEntry {
		MyRentables, MyRents, MyBank, Disconnect, Back
	}

	private ConnectionResponse connection;

	private final IUsersController users;
	private final IRentsController rents;
	private final IRentablesController rentables;

	private final Menu<MyAccountEntry> myAccount = new TitledMenu<>("=== Mon compte ===");

	public MyAccountController(ConnectionResponse connection, IUsersController users, IRentsController rents, IRentablesController rentables) {
		this.connection = Objects.requireNonNull(connection, "Invalid connection.");
		this.users = Objects.requireNonNull(users, "Invalid users.");
		this.rents = Objects.requireNonNull(rents, "Invalid rents.");
		this.rentables = Objects.requireNonNull(rentables, "Invalid rentables.");

		myAccount.addEntry("Mes louables.", MyAccountEntry.MyRentables)
			.addEntry("Mes locations.", MyAccountEntry.MyRents)
			.addEntry("Mon compte bancaire.", MyAccountEntry.MyBank)
			.addEntry("Me déconnecter.", MyAccountEntry.Disconnect)
			.addEntry("Retour.", MyAccountEntry.Back);
	}

	@Override
	public Boolean show(Scanner input) {
		MyAccountEntry choice = null;

		do {
			choice = myAccount.show(input);

			switch (choice) {
			case MyRentables :
				myRentables(input);
				break;
			case MyRents :
				myRents(input);
				break;
			case MyBank :
				myBank(input);
				break;
			case Disconnect :
				disconnect(input);
				return false;
			case Back :
				break;
			}
		} while (choice != MyAccountEntry.Back);
		
		return true;
	}
	
	private void myRentables(Scanner input) {
		MyRentablesController myRentables = new MyRentablesController(connection, rentables);
		
		myRentables.start(input);
	}

	private void myRents(Scanner input) {
		MyRentsController myRents = new MyRentsController(connection, users, rents, rentables);
		
		myRents.start(input);
	}

	private void myBank(Scanner input) {
		MyBankController myBank = new MyBankController(connection, users);
		
		myBank.start(input);
	}

	private void disconnect(Scanner input) {
		try {
			users.disconnect(connection.getTicket());
			ColoredOutput.println("Vous avez été déconnecté" + (connection.getUser().getElement().getTitle() == Title.Mrs ? "e" : "") + ".\n", OutputColor.Green);
		} catch (RemoteException e) {
			// DO NOTHING
			// TODO : disable local ticket
		}
	}
}
