package fr.upem.mlvcars.controllers.account;

import java.rmi.RemoteException;
import java.util.Objects;
import java.util.Optional;
import java.util.Scanner;

import javax.xml.rpc.ServiceException;

import fr.upem.mlvbank.BankAccountSnapShot;
import fr.upem.mlvbank.BankService;
import fr.upem.mlvbank.BankServiceServiceLocator;
import fr.upem.mlvbank.Transaction;
import fr.upem.mlvcars.controllers.users.ConnectionResponse;
import fr.upem.mlvcars.controllers.users.IUsersController;
import fr.upem.mlvcars.menus.InputTools;
import fr.upem.mlvcars.menus.Menu;
import fr.upem.mlvcars.menus.MenuController;
import fr.upem.mlvcars.menus.TitledMenu;
import fr.upem.output.ColoredOutput;
import fr.upem.output.ColoredOutput.OutputColor;

public class MyBankController implements MenuController {
	public static enum MyBankOperation {
		GetBalance, Debit, Deposit, Back
	}

	private final ConnectionResponse connection;

	private final IUsersController users;

	private final Menu<MyBankOperation> myBank = new TitledMenu<>("=== Mon compte bancaire ===");

	public MyBankController(ConnectionResponse connection, IUsersController users) {
		this.connection = Objects.requireNonNull(connection, "Invalid connection.");
		this.users = Objects.requireNonNull(users, "Invalid users.");

		myBank.addEntry("Afficher le solde.", MyBankOperation.GetBalance)
			.addEntry("Retirer de l'argent.", MyBankOperation.Debit)
			.addEntry("Déposer de l'argent.", MyBankOperation.Deposit)
			.addEntry("Retour.", MyBankOperation.Back);
	}

	@Override
	public void start(Scanner input) {
		MyBankOperation choice = null;

		do {
			choice = myBank.show(input);
			
			switch (choice) {
			case GetBalance :
				getBalance();
				break;
			case Debit :
				debit(input);
				break;
			case Deposit :
				deposit(input);
				break;
			case Back :
				break;
			}
		} while (choice != MyBankOperation.Back);
	}

	private void getBalance() {
		ColoredOutput.println("=== Solde du compte ===\n");
		
		try {
			BankService bank = new BankServiceServiceLocator().getBankService();
			long number = users.getUserBankAccount(connection.getTicket());
			
			BankAccountSnapShot snap = bank.getSnapShot(number);
			
			ColoredOutput.print("" + (snap.getBalance() / 100.) + " ", OutputColor.Yellow);
			ColoredOutput.println(snap.getCurrency() + " [Compte " + snap.getNumber() + "]\n");
		} catch (IllegalStateException e) {
			ColoredOutput.println("Vous n'êtes pas connecté.\n", OutputColor.Red);
		} catch (RemoteException | ServiceException e) {
			ColoredOutput.println("Erreur de connexion.\n", OutputColor.Red);
		}
	}

	private void debit(Scanner input) {
		ColoredOutput.println("=== Débit ===\n");
		
		try {
			ColoredOutput.print("Somme (en centimes) : ");
			Optional<Integer> amount = InputTools.getInt(input);
			
			if (amount.isPresent()) {
				BankService bank = new BankServiceServiceLocator().getBankService();
				long number = users.getUserBankAccount(connection.getTicket());
				
				BankAccountSnapShot snap = bank.getSnapShot(number);
				int result = bank.transact(new Transaction(number, amount.get(), snap.getCurrency(), 0));
				
				if (result == 0) {
					ColoredOutput.println("Vous avez retiré " + (amount.get() / 100.) + " " + snap.getCurrency() + ".\n", OutputColor.Green);
				} else {
					ColoredOutput.println("Vous n'avez pas les fonds nécessaires.\n", OutputColor.Red);
				}
			} else {
				ColoredOutput.println("Somme saisie invalide. Abandon.\n", OutputColor.Red);
			}
		} catch (IllegalStateException e) {
			ColoredOutput.println("Vous n'êtes pas connecté.\n", OutputColor.Red);
		} catch (RemoteException | ServiceException e) {
			ColoredOutput.println("Erreur de connexion.\n", OutputColor.Red);
		}
	}

	private void deposit(Scanner input) {
		ColoredOutput.println("=== Dépôt ===\n");
		
		try {
			ColoredOutput.print("Somme (en centimes) : ");
			Optional<Integer> amount = InputTools.getInt(input);
			
			if (amount.isPresent()) {
				BankService bank = new BankServiceServiceLocator().getBankService();
				long number = users.getUserBankAccount(connection.getTicket());
				
				BankAccountSnapShot snap = bank.getSnapShot(number);
				int result = bank.transact(new Transaction(number, amount.get(), snap.getCurrency(), 1));
				
				if (result == 0) {
					ColoredOutput.println("Vous avez déposé " + (amount.get() / 100.) + " " + snap.getCurrency() + ".\n", OutputColor.Green);
				} else {
					ColoredOutput.println("Dépôt non effectué.\n", OutputColor.Red);
				}
			} else {
				ColoredOutput.println("Somme saisie invalide. Abandon.\n", OutputColor.Red);
			}
		} catch (IllegalStateException e) {
			ColoredOutput.println("Vous n'êtes pas connecté.\n", OutputColor.Red);
		} catch (RemoteException | ServiceException e) {
			ColoredOutput.println("Erreur de connexion.\n", OutputColor.Red);
		}	
	}
}
