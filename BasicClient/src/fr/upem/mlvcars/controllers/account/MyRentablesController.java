package fr.upem.mlvcars.controllers.account;

import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Scanner;
import java.util.function.BiConsumer;

import fr.upem.mlvcars.controllers.rentables.CarModificationController;
import fr.upem.mlvcars.controllers.rentables.IRentablesController;
import fr.upem.mlvcars.controllers.rentables.RentableData;
import fr.upem.mlvcars.controllers.rentables.RentablesCreationController;
import fr.upem.mlvcars.controllers.rentables.RentablesSelectionController;
import fr.upem.mlvcars.controllers.users.ConnectionResponse;
import fr.upem.mlvcars.menus.Menu;
import fr.upem.mlvcars.menus.MenuController;
import fr.upem.mlvcars.menus.TitledMenu;
import fr.upem.mlvcars.rentables.RentableType;
import fr.upem.output.ColoredOutput;
import fr.upem.output.ColoredOutput.OutputColor;

public class MyRentablesController implements MenuController {
	public static enum MyRentablesEntry {
		CreateOne, SelectOne, Refresh, Back
	}
	
	public static enum RentableOperationEntry {
		Modify, Abort
	}

	private final ConnectionResponse connection;

	private final IRentablesController rentables;

	private final Menu<MyRentablesEntry> myRentables = new TitledMenu<>("=== Mes louables ===");
	private final Menu<RentableOperationEntry> rentableOperations = new TitledMenu<RentableOperationEntry>("=== Choisir une opération ===");
	
	private final Map<RentableType, BiConsumer<Scanner, RentableData>> modifiers = new HashMap<>();

	public MyRentablesController(ConnectionResponse connection, IRentablesController rentables) {
		this.connection = Objects.requireNonNull(connection, "Invalid connection.");
		this.rentables = Objects.requireNonNull(rentables, "Invalid rentables.");

		myRentables.addEntry("Créer un louable.", MyRentablesEntry.CreateOne)
			.addEntry("Rafraîchir la liste.", MyRentablesEntry.Refresh)
			.addEntry("Retour.", MyRentablesEntry.Back);
		
		rentableOperations.addEntry("Modifier.", RentableOperationEntry.Modify)
			.addEntry("Annuler.", RentableOperationEntry.Abort);
		
		modifiers.put(RentableType.Car, (in, r) -> new CarModificationController(connection, rentables, r).start(in));
	}

	@Override
	public void start(Scanner input) {
		MyRentablesEntry choice = null;

		do {
			try {
				myRentables.addEntryIfAbsent("Sélectionner un louable.", MyRentablesEntry.SelectOne, 1);
				
				List<RentableData> userRentables = rentables.getRentablesForUser(connection.getTicket());

				if (userRentables.isEmpty()) {
					ColoredOutput.println("Vous n'avez aucun louable.\n");
					myRentables.removeEntry(1);
				} else {
					userRentables.forEach(e -> ColoredOutput.println(e.getRentable().getElement() + " [" + (e.getPrice() / 100.) + e.getCurrency().getCurrencyCode() + "/heure]"));
					ColoredOutput.println();
				}

				choice = myRentables.show(input);

				switch (choice) {
				case CreateOne:
					createRentable(input);
					break;
				case SelectOne:
					selectRentable(input, userRentables);
					break;
				default:
					break;
				}
			} catch (IllegalStateException e) {
				ColoredOutput.println("Vous n'êtes pas connecté.\n", OutputColor.Red);
				return;
			} catch (RemoteException e) {
				ColoredOutput.println("Erreur de connexion.\n", OutputColor.Red);
				return;
			}
		} while (choice != MyRentablesEntry.Back);
	}

	private void createRentable(Scanner input) {
		RentablesCreationController creator = new RentablesCreationController(connection, rentables);
		
		creator.start(input);
	}

	private void selectRentable(Scanner input, List<RentableData> userRentables) {
		RentablesSelectionController selector = new RentablesSelectionController(connection, rentables, userRentables);
		
		RentableData rentable = selector.show(input);
		
		if (rentable != null) {
			selectOperationForRentable(input, rentable);
		}
	}
	
	private void selectOperationForRentable(Scanner input, RentableData rentable) {
		RentableOperationEntry operationChoice = rentableOperations.show(input);
		
		switch (operationChoice) {
		case Modify:
			modifyRentable(input, rentable);
			break;
		case Abort:
			return;
		}
	}

	private void modifyRentable(Scanner input, RentableData selectedRentable) {
		BiConsumer<Scanner, RentableData> modifier = modifiers.get(selectedRentable.getRentable().getElement().getType());
		
		if (modifier == null) {
			ColoredOutput.println("Impossible de modifier un louable de type " + selectedRentable.getRentable().getElement().getType().getTitle() + "\n", OutputColor.Red);
		} else {
			modifier.accept(input, selectedRentable);
		}
	}
}
