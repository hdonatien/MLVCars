package fr.upem.mlvcars.controllers.account;

import java.rmi.RemoteException;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Scanner;

import fr.upem.mlvbank.BankAccountSnapShot;
import fr.upem.mlvbank.BankService;
import fr.upem.mlvbank.BankServiceServiceLocator;
import fr.upem.mlvcars.controllers.rentables.IRentablesController;
import fr.upem.mlvcars.controllers.rents.CancelResponse;
import fr.upem.mlvcars.controllers.rents.ExtensionResponse;
import fr.upem.mlvcars.controllers.rents.FinishResponse;
import fr.upem.mlvcars.controllers.rents.IRentsController;
import fr.upem.mlvcars.controllers.rents.RentsSelectionController;
import fr.upem.mlvcars.controllers.users.ConnectionResponse;
import fr.upem.mlvcars.controllers.users.IUsersController;
import fr.upem.mlvcars.menus.BasicMenus;
import fr.upem.mlvcars.menus.BasicMenus.ConfirmResult;
import fr.upem.mlvcars.menus.InputTools;
import fr.upem.mlvcars.menus.Menu;
import fr.upem.mlvcars.menus.MenuController;
import fr.upem.mlvcars.menus.TitledMenu;
import fr.upem.output.ColoredOutput;
import fr.upem.output.ColoredOutput.OutputColor;
import fr.upem.rate.Rate;
import fr.upem.rent.RentsManager.Renting;
import fr.upem.store.Entity;

public class MyRentsController implements MenuController {
	public static enum MyRentsEntry {
		SelectOne, Refresh, Back
	}
	
	public static enum RentOperationEntry {
		Cancel, Finish, Extend, Abort
	}

	private final ConnectionResponse connection;

	private final IUsersController users;
	private final IRentsController rents;
	private final IRentablesController rentables;

	private final Menu<MyRentsEntry> myRents = new TitledMenu<>("=== Mes locations ===");
	private final Menu<RentOperationEntry> rentableOperations = new TitledMenu<>("=== Choisir une opération ===");

	public MyRentsController(ConnectionResponse connection, IUsersController users, IRentsController rents, IRentablesController rentables) {
		this.connection = Objects.requireNonNull(connection, "Invalid connection.");
		this.users = Objects.requireNonNull(users, "Invalid users.");
		this.rents = Objects.requireNonNull(rents, "Invalid rents.");
		this.rentables = Objects.requireNonNull(rentables, "Invalid rentables.");

		myRents.addEntry("Rafraîchir la liste.", MyRentsEntry.Refresh)
			.addEntry("Retour.", MyRentsEntry.Back);
		
		rentableOperations.addEntry("Annuler la location.", RentOperationEntry.Cancel)
			.addEntry("Déclarer la fin.", RentOperationEntry.Finish)
			.addEntry("Étendre.", RentOperationEntry.Extend)
			.addEntry("Annuler.", RentOperationEntry.Abort);
	}

	@Override
	public void start(Scanner input) {
		MyRentsEntry choice = null;

		do {
			try {
				myRents.addEntryIfAbsent("Sélectionner une location.", MyRentsEntry.SelectOne, 1);
				
				List<Entity<Renting>> userRents = rents.getRentsForUser(connection.getTicket());

				if (userRents.isEmpty()) {
					ColoredOutput.println("Vous n'avez aucune location.\n");
					myRents.removeEntry(1);
				} else {
					try {
						BankService bank = new BankServiceServiceLocator().getBankService();
						long number = users.getUserBankAccount(connection.getTicket());
						BankAccountSnapShot snap = bank.getSnapShot(number);
						
						for (Entity<Renting> renting : userRents) {
							Renting r = renting.getElement();
							long realPrice = bank.convert("EUR", snap.getCurrency(), renting.getElement().getPrice());
							
							ColoredOutput.println(r.getRent() + " -> " + r.getRentable() + " [" + (realPrice / 100.) + snap.getCurrency() + "/heure]");
						}
	
						ColoredOutput.println();
					} catch (Exception e) {
						ColoredOutput.println("Erreur de connexion.\n", OutputColor.Red);
					}
				}

				choice = myRents.show(input);

				switch (choice) {
				case SelectOne:
					selectRent(input, userRents);
					break;
				default:
					break;
				}
			} catch (IllegalStateException e) {
				ColoredOutput.println("Vous n'êtes pas connecté.\n", OutputColor.Red);
				return;
			} catch (RemoteException e) {
				ColoredOutput.println("Erreur de connexion.\n", OutputColor.Red);
				return;
			}
		} while (choice != MyRentsEntry.Back);
	}

	private void selectRent(Scanner input, List<Entity<Renting>> userRents) {
		RentsSelectionController selector = new RentsSelectionController(connection, rentables, userRents);
		
		Entity<Renting> selected = selector.show(input);
		
		if (selected != null) {
			selectOperation(input, selected);
		}
	}

	private void selectOperation(Scanner input, Entity<Renting> rent) {
		RentOperationEntry operationChoice = null;
		
		do {
			operationChoice = rentableOperations.show(input);
		
			switch (operationChoice) {
			case Cancel:
				cancelRent(input, rent);
				break;
			case Finish :
				finishRent(input, rent);
				break;
			case Extend :
				extendRent(input, rent);
				break;
			case Abort:
				return;
			}
		} while (operationChoice != RentOperationEntry.Abort);
	}

	private void cancelRent(Scanner input, Entity<Renting> rent) {
		ConfirmResult confirm = BasicMenus.confirm.show(input);
		
		if (confirm == ConfirmResult.Yes) {
			try {
				CancelResponse response = rents.cancelRent(connection.getTicket(), rent.getId());
				
				if (response == CancelResponse.Success) {
					ColoredOutput.println("Location annulée.\n", OutputColor.Green);
				} else {
					ColoredOutput.println("Impossible d'annuler la location.\n", OutputColor.Red);
				}
			} catch (IllegalStateException e) {
				ColoredOutput.println("Vous n'êtes pas connecté.\n", OutputColor.Red);
			} catch (RemoteException e) {
				ColoredOutput.println("Erreur de connexion.\n", OutputColor.Red);
			}
		} else {
			ColoredOutput.println("Location toujours active.\n");
		}
	}

	private void finishRent(Scanner input, Entity<Renting> rent) {
		ColoredOutput.println("Vous pouvez laisser un commentaire.");
		ConfirmResult confirm = BasicMenus.confirm.show(input);
		Rate rate = null;
		
		if (confirm == ConfirmResult.Yes) {
			rate = addComment(input, rent);
		}
		
		try {
			FinishResponse response = rents.finishRent(connection.getTicket(), rent.getId(), rate);
			
			if (response == FinishResponse.Success) {
				ColoredOutput.println("Location déclarée terminée.\n", OutputColor.Green);
			} else {
				ColoredOutput.println("Impossible de déclarer la fin de la location.\n", OutputColor.Red);
			}
		} catch (IllegalStateException e) {
			ColoredOutput.println("Vous n'êtes pas connecté.\n", OutputColor.Red);
		} catch (RemoteException e) {
			ColoredOutput.println("Erreur de connexion.\n", OutputColor.Red);
		}
	}

	private Rate addComment(Scanner input, Entity<Renting> rent) {
		ColoredOutput.println("=== Ajout d'un commentaire ===");
		
		ColoredOutput.print("Note (sur 10) : ");
		Optional<Integer> mark = InputTools.getInt(input);
		
		ColoredOutput.print("Commentaire : ");
		Optional<String> comment = InputTools.getString(input);
		
		if (!mark.isPresent() || comment.isPresent() || !(mark.get() >= 0 && mark.get() <= 10)) {
			int markValue = mark.get();
			String commentValue = comment.get().trim();
			
			return new Rate(commentValue.length() == 0 ? "Aucun commentaire n'a été laissé par le client." : commentValue, markValue);
		} else {
			ColoredOutput.println("Les données saisies sont invalides. Abandon.\n", OutputColor.Red);
			return null;
		}
	}

	private void extendRent(Scanner input, Entity<Renting> rent) {
		ColoredOutput.println("=== Extension de location ===");
		
		Optional<ZonedDateTime> newEnd = InputTools.getDate(input);
		
		if (!newEnd.isPresent()) {
			ZonedDateTime end = newEnd.get();
			try {
				ExtensionResponse response = rents.extendRent(connection.getTicket(), rent.getId(), end);
				
				if (response == ExtensionResponse.Success) {
					ColoredOutput.println("Location étendue avec succès.\n", OutputColor.Green);
				} else {
					ColoredOutput.println("Impossible d'étendre la location. Il se peut que la nouvelle période ne soit pas disponible ou que vous n'ayez pas les fonds nécessaires.\n", OutputColor.Red);
				}
			} catch (IllegalStateException e) {
				ColoredOutput.println("Vous n'êtes pas connecté.\n", OutputColor.Red);
			} catch (RemoteException e) {
				ColoredOutput.println("Erreur de connexion.\n", OutputColor.Red);
			}
		} else {
			ColoredOutput.println("Les données saisies sont invalides. Abandon.\n", OutputColor.Red);
		}
	}
}
