package fr.upem.mlvcars.controllers.login;

import java.rmi.RemoteException;
import java.util.Objects;
import java.util.Scanner;

import fr.upem.mlvcars.controllers.login.ConnectController.ConnectResult;
import fr.upem.mlvcars.controllers.users.ConnectionResponse;
import fr.upem.mlvcars.controllers.users.IUsersController;
import fr.upem.mlvcars.menus.BasicMenus;
import fr.upem.mlvcars.menus.BasicMenus.ConfirmResult;

public class LoginController {
	private final IUsersController users;
	
	private final ConnectController connector;
	
	private ConnectionResponse connection;

	public LoginController(IUsersController users) {
		this.users = Objects.requireNonNull(users, "Invalid users controller.");
		this.connector = new ConnectController(users);
	}

	public boolean connect(Scanner input) {
		ConnectResult result = null;
		ConfirmResult retry = null;
		
		do {
			result = connector.show(input);
			
			if (result.getCode() != ConnectResult.Code.Success) {
				retry = BasicMenus.retry.show(input);
			}
		} while (result.getCode() != ConnectResult.Code.Success && retry == ConfirmResult.Yes);
		
		if (result.getCode() == ConnectResult.Code.Success) {
			connection = result.getResponse();
			
			return true;
		}
		
		return false;
	}
	
	public boolean userIsConnected() {
		return connection != null;
	}
	
	public ConnectionResponse getConnection() throws IllegalStateException {
		checkUserConnection();
		
		return connection;
	}
	
	public void disconnect(Scanner input) {
		try {
			if (BasicMenus.confirm.show(input) == ConfirmResult.Yes) {
				users.disconnect(connection.getTicket());
				emptyConnection();
			}
		} catch (IllegalStateException e) {
			// DO NOTHING
		} catch (RemoteException e) {
			emptyConnection();
		}
	}

	private void checkUserConnection() throws IllegalStateException {
		if (connection == null) {
			throw new IllegalStateException("User is not connected.");
		}
	}
	
	private void emptyConnection() {
		connection = null;
	}
}
