package fr.upem.mlvcars.controllers.login;

import java.rmi.RemoteException;
import java.util.Objects;
import java.util.Optional;
import java.util.Scanner;

import fr.upem.contract.Contract;
import fr.upem.mlvcars.controllers.users.ConnectionResponse;
import fr.upem.mlvcars.controllers.users.IUsersController;
import fr.upem.mlvcars.menus.InputController;
import fr.upem.mlvcars.menus.InputTools;
import fr.upem.output.ColoredOutput;
import fr.upem.output.ColoredOutput.OutputColor;

public class ConnectController implements InputController<ConnectController.ConnectResult> {
	public static class ConnectResult {
		public static enum Code {
			Success, Fail, NetworkError, Aborted
		}

		private final Code code;
		private final ConnectionResponse response;

		public ConnectResult(Code code) throws IllegalArgumentException {
			Objects.requireNonNull(code, "Invalid code.");

			this.code = Contract.checkThat(code, c -> c != Code.Success, "Code can't be a success code with no connection response.");
			this.response = null;
		}

		public ConnectResult(ConnectionResponse response) throws IllegalArgumentException {
			Objects.requireNonNull(response, "Invalid connection response.");

			this.response = Contract.checkThat(response, r -> r.isSuccessful(), "Connection response is not successful.");
			this.code = Code.Success;
		}

		public Code getCode() {
			return code;
		}
		
		public ConnectionResponse getResponse() throws IllegalStateException {
			if (code != Code.Success) {
				throw new IllegalStateException("Connection response is unavailable.");
			}
			
			return response;
		}
	}
	
	private final IUsersController users;
	
	public ConnectController(IUsersController users) {
		this.users = Objects.requireNonNull(users, "Invalid users controller.");
	}
	
	@Override
	public ConnectResult show(Scanner input) throws IllegalStateException {
		Optional<String> login = null, password = null;
		
		ColoredOutput.print("Login : ");
		login = InputTools.getString(input);

		ColoredOutput.print("Mot de passe : ");
		password = InputTools.getString(input);

		if (login.isPresent() && password.isPresent()) {
			try {
				ConnectionResponse response = users.connect(login.get(), password.get());

				if (response.isSuccessful()) {
					ColoredOutput.println("Connexion réussie.\n", OutputColor.Green);

					return new ConnectResult(response);
				} else {
					ColoredOutput.println("Identifiant ou mot de passe invalide.\n", OutputColor.Red);

					return new ConnectResult(ConnectResult.Code.Fail);
				}
			} catch (RemoteException e) {
				return new ConnectResult(ConnectResult.Code.NetworkError);
			}
		}
		
		return new ConnectResult(ConnectResult.Code.Aborted);
	}
}
