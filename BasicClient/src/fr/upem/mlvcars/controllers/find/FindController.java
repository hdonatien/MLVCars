package fr.upem.mlvcars.controllers.find;

import java.rmi.RemoteException;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;

import fr.upem.mlvcars.controllers.rentables.IRentablesController;
import fr.upem.mlvcars.controllers.rentables.RentableData;
import fr.upem.mlvcars.controllers.rentables.RentableTypesSelectionController;
import fr.upem.mlvcars.controllers.rentables.RentablesSelectionController;
import fr.upem.mlvcars.controllers.rents.IRentsController;
import fr.upem.mlvcars.controllers.rents.RentingController;
import fr.upem.mlvcars.controllers.users.ConnectionResponse;
import fr.upem.mlvcars.menus.Menu;
import fr.upem.mlvcars.menus.MenuController;
import fr.upem.mlvcars.menus.TitledMenu;
import fr.upem.mlvcars.rentables.RentableType;
import fr.upem.output.ColoredOutput;
import fr.upem.output.ColoredOutput.OutputColor;
import fr.upem.rate.Rate;
import fr.upem.store.Entity;

public class FindController implements MenuController {
	private static enum OperationOnPublicRentable {
		Rent, SeeComments, Abort
	}
	
	private final ConnectionResponse connection;
	
	private final IRentsController rents;
	private final IRentablesController rentables;
	
	private final RentableTypesSelectionController typeSelector;
	
	private final Menu<OperationOnPublicRentable> operationSelector = new TitledMenu<>("=== Sélectionner une opération ===");
	
	public FindController(ConnectionResponse connection, IRentsController rents, IRentablesController rentables) {
		this.connection = Objects.requireNonNull(connection, "Invalid connection.");
		this.rents = Objects.requireNonNull(rents, "Invalid rents.");
		this.rentables = Objects.requireNonNull(rentables, "Invalid rentables.");
		this.typeSelector = new RentableTypesSelectionController();
		operationSelector.addEntry("Louer.", OperationOnPublicRentable.Rent)
			.addEntry("Voir les commentaires.", OperationOnPublicRentable.SeeComments)
			.addEntry("Annuler.", OperationOnPublicRentable.Abort);
	}
	
	@Override
	public void start(Scanner input) {
		RentableType type = null;
		
		do {
			type = typeSelector.show(input);
		
			if (type != null) {
				selectRentable(input, type);
			}
		} while (type != null);
	}
	
	private void selectRentable(Scanner input, RentableType type) {
		try {
			RentableData rentable = null;
			
			do {
				List<RentableData> rentablesOfType = rentables.getFilteredRentables(connection.getTicket(), type);
				
				RentablesSelectionController rentableSelector = new RentablesSelectionController(connection, rentables, rentablesOfType);
				rentable = rentableSelector.show(input);
				
				if (rentable != null) {
					selectOperation(input, rentable);
				}
			} while (rentable != null);
		} catch (IllegalStateException e) {
			ColoredOutput.println("Vous n'êtes pas connecté.\n", OutputColor.Red);
		} catch (RemoteException e) {
			ColoredOutput.println("Erreur de connexion.\n", OutputColor.Red);
		}
	}
	
	private void selectOperation(Scanner input, RentableData rentable) {
		OperationOnPublicRentable operation = null;
		
		do {
			operation = operationSelector.show(input);
			
			switch (operation) {
			case Rent :
				rent(input, rentable);
				break;
			case SeeComments :
				seeComments(input, rentable);
				break;
			case Abort :
				return;
			}
		} while (operation == OperationOnPublicRentable.SeeComments);
	}

	private void rent(Scanner input, RentableData rentable) {
		RentingController rentor = new RentingController(connection, rents, rentable);
		rentor.start(input);
	}

	private void seeComments(Scanner input, RentableData rentable) {
		try {
			List<Entity<Rate>> rates = rentables.getComments(connection.getTicket(), rentable.getRentable().getId());
			
			if (rates.isEmpty()) {
				ColoredOutput.println("Aucun commentaire pour le moment.");
			} else {
				rates.forEach(r -> {
					ColoredOutput.print("[" + (r.getElement().getMark() / 2.) + "/5] ", OutputColor.Yellow);
					ColoredOutput.println(r.getElement().getComment().trim());
				});
			}
			
			ColoredOutput.println();
		} catch (IllegalStateException e) {
			ColoredOutput.println("Vous n'êtes pas connecté.\n", OutputColor.Red);
		} catch (RemoteException e) {
			ColoredOutput.println("Erreur de connexion.\n", OutputColor.Red);
		}
	}
}
