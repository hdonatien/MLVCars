package fr.upem.mlvcars.menus;

import java.util.Scanner;

public interface InputController<T> {
	public T show(Scanner input) throws IllegalStateException;
}
