package fr.upem.mlvcars.menus;

import java.time.YearMonth;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Optional;
import java.util.Scanner;

import fr.upem.output.ColoredOutput;

public class InputTools {
	public static Optional<String> getString(Scanner input) throws IllegalStateException {
		try {
			if (input.hasNextLine()) {
				return Optional.<String>of(input.nextLine());
			}

			throw new IllegalStateException("Invalid input stream.");
		} catch (Exception e) {
			return Optional.<String>empty();
		}
	}

	public static Optional<Integer> getInt(Scanner input) throws IllegalStateException {
		try {
			if (input.hasNextLine()) {
				return Optional.<Integer>of(Integer.parseInt(input.nextLine()));
			}

			throw new IllegalStateException("Invalid input stream.");
		} catch (Exception e) {
			return Optional.<Integer>empty();
		}
	}

	public static Optional<Long> getLong(Scanner input) throws IllegalStateException {
		try {
			if (input.hasNextLine()) {
				return Optional.<Long>of(Long.parseLong(input.nextLine()));
			}

			throw new IllegalStateException("Invalid input stream.");
		} catch (Exception e) {
			return Optional.<Long>empty();
		}
	}
	
	public static Optional<ZonedDateTime> getDate(Scanner input) throws IllegalStateException {
		try {
			ColoredOutput.print("Année (" + ZonedDateTime.now().getYear() + "-?) : ");
			Optional<Integer> year = getInt(input);
			
			if (!year.isPresent()) {
				return Optional.<ZonedDateTime>empty();
			}
			
			ColoredOutput.print("Mois (1-12) : ");
			Optional<Integer> month = getInt(input);
			
			if (!month.isPresent() || month.get() < 1 || month.get() > 12) {
				return Optional.<ZonedDateTime>empty();
			}
			
			YearMonth yearMonthObject = YearMonth.of(year.get(), month.get());
			int daysInMonth = yearMonthObject.lengthOfMonth();
			
			ColoredOutput.print("Jour (1-" + daysInMonth + ") : ");
			Optional<Integer> day = getInt(input);
			
			if (!day.isPresent() || day.get() < 1 || day.get() > daysInMonth) {
				return Optional.<ZonedDateTime>empty();
			}
			
			ColoredOutput.print("Heure (0-23) : ");
			Optional<Integer> hour = getInt(input);
			
			if (!hour.isPresent() || hour.get() < 0 || hour.get() > 23) {
				return Optional.<ZonedDateTime>empty();
			}
			
			ColoredOutput.print("Minute (0-59) : ");
			Optional<Integer> minute = getInt(input);
			
			if (!minute.isPresent() || minute.get() < 0 || minute.get() > 59) {
				return Optional.<ZonedDateTime>empty();
			}
			
			return Optional.<ZonedDateTime>of(ZonedDateTime.of(year.get(), month.get(), day.get(), hour.get(), minute.get(), 0, 0, ZoneId.of("UTC")));
		} catch (Exception e) {
			return Optional.<ZonedDateTime>empty();
		}
	}
}
