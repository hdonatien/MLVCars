package fr.upem.mlvcars.menus;

public class BasicMenus {
	public static enum ConfirmResult {
		Yes, No
	}

	public static final Menu<ConfirmResult> retry = new TitledMenu<ConfirmResult>("Réessayer ?")
		.addEntry("Oui.", ConfirmResult.Yes).addEntry("Non.", ConfirmResult.No);

	public static final Menu<ConfirmResult> confirm = new TitledMenu<ConfirmResult>("Confirmez-vous ?")
		.addEntry("Oui.", ConfirmResult.Yes).addEntry("Non.", ConfirmResult.No);
}
