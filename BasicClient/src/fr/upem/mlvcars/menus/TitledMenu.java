package fr.upem.mlvcars.menus;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Scanner;
import java.util.stream.IntStream;

import fr.upem.output.ColoredOutput;
import fr.upem.output.ColoredOutput.OutputColor;

public class TitledMenu<T> implements Menu<T> {
	private final String title;
	private final Map<String, T> entries = new HashMap<>();
	private final List<String> entriesOrder = new LinkedList<>();
	
	public TitledMenu(String title) {
		this.title = Objects.requireNonNull(title, "Invalid menu title.");
	}

	@Override
	public Menu<T> addEntry(String text, T code) {
		return addEntry(text, code, entries.size());
	}

	@Override
	public Menu<T> addEntry(String text, T code, int index) {
		Objects.requireNonNull(text, "Invalid entry text.");

		if (entries.containsKey(text)) {
			throw new IllegalArgumentException("Menu already contains entry '" + text + "'");
		}

		entries.put(text, code);
		entriesOrder.add(index, text);

		return this;
	}

	@Override
	public void addEntryIfAbsent(String text, T code) {
		addEntryIfAbsent(text, code, entries.size());
	}

	@Override
	public void addEntryIfAbsent(String text, T code, int index) {
		Objects.requireNonNull(text, "Invalid entry text.");
		
		if (!entries.containsKey(text)) {
			entries.put(text, code);
			entriesOrder.add(index, text);
		}
	}

	@Override
	public Menu<T> removeEntry(int index) {
		entries.remove(entriesOrder.get(index));
		entriesOrder.remove(index);

		return this;
	}

	@Override
	public T show(Scanner input) {
		Optional<Integer> choice = null;

		printMenu();

		while (true) {
			ColoredOutput.print("> ");
			choice = InputTools.getInt(input);

			if (choice.isPresent()) {
				int choiceValue = choice.get();

				if (!isValidChoice(choiceValue)) {
					ColoredOutput.println("Choix invalide.", OutputColor.Red);
				} else {
					ColoredOutput.println();

					return entries.get(entriesOrder.get(choiceValue - 1));
				}
			}
		}
	}

	private void printMenu() {
		ColoredOutput.println(title);
		IntStream.range(0, entries.size()).forEach(this::printEntry);
	}

	private void printEntry(int index) {
		ColoredOutput.println((index + 1) + ". " + entriesOrder.get(index));
	}

	private boolean isValidChoice(int choice) {
		return choice >= 1 && choice <= entries.size();
	}
}
