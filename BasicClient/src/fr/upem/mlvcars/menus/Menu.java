package fr.upem.mlvcars.menus;

import java.util.Scanner;

public interface Menu<T> {
	Menu<T> addEntry(String text, T code);
	Menu<T> addEntry(String text, T code, int index);
	void addEntryIfAbsent(String text, T code);
	void addEntryIfAbsent(String text, T code, int index);
	Menu<T> removeEntry(int index);
	T show(Scanner input) throws IllegalStateException;
}
