package fr.upem.mlvcars.menus;

import java.util.Scanner;

public interface MenuController {
	void start(Scanner input);
}
