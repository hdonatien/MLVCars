package fr.upem.mlvcars;

import java.nio.file.Paths;
import java.util.Objects;
import java.util.Scanner;

import fr.upem.mlvcars.controllers.login.LoginController;
import fr.upem.mlvcars.controllers.mainmenu.MainController;
import fr.upem.mlvcars.controllers.rentables.IRentablesController;
import fr.upem.mlvcars.controllers.rents.IRentsController;
import fr.upem.mlvcars.controllers.users.IUsersController;
import fr.upem.mlvcars.data.DataAccess;
import fr.upem.mlvcars.users.UserType;
import fr.upem.output.ColoredOutput;
import fr.upem.output.ColoredOutput.OutputColor;
import fr.upem.user.User;

public class Client {
	private final IUsersController users;
	private final IRentsController rents;
	private final IRentablesController rentables;

	private final Scanner input = new Scanner(System.in);
	
	private final LoginController login;

	public Client(IUsersController users, IRentsController rents, IRentablesController rentables) {
		this.users = Objects.requireNonNull(users, "Invalid users.");
		this.rents = Objects.requireNonNull(rents, "Invalid rents.");
		this.rentables = Objects.requireNonNull(rentables, "Invalid rentables.");
		
		this.login = new LoginController(users);
	}

	public void start() {
		if (login.connect(input)) {
			User<UserType> user = login.getConnection().getUser().getElement();
			ColoredOutput.println("Bienvenue, " + user.getFirstName() + " " + user.getLastName() + " !\n");
			
			new MainController(login.getConnection(), users, rents, rentables).start(input);
		} else {
			ColoredOutput.println("Le programme quitte...");
		}
	}

	public static void main(String[] args) {
		try {
			String host = args[0];
			int port = Integer.parseInt(args[1]);
			String shared = args[2];
			String policy = args[3];
			
			DataAccess data = DataAccess.getInstance(host, port, Paths.get(shared), Paths.get(policy));
			
			IUsersController users = data.getUsersController();
			IRentsController rents = data.getRentsController();
			IRentablesController rentables = data.getRentablesController();

			new Client(users, rents, rentables).start();
		} catch (Exception e) {
			ColoredOutput.println("Une erreur est survenue. Abandon. " + e, OutputColor.Red);
		}
	}
}
