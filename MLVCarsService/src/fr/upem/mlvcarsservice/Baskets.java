package fr.upem.mlvcarsservice;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import fr.upem.mlvcars.controllers.rentables.Buyable;

public class Baskets {
	private static Baskets baskets;
	private static final Object monitor = new Object();
	
	public static Baskets getInstance() {
		synchronized (monitor) {
			if (baskets == null) {
				baskets = new Baskets();
			}
			
			return baskets;
		}
	}
	
	private final Map<String, List<Buyable>> rentablesByBasket = new HashMap<>();
	
	private Baskets() {
		
	}
	
	public void create(String user) {
		rentablesByBasket.put(user, new LinkedList<>());
	}
	
	public List<Buyable> get(String user) {
		List<Buyable> rentables = rentablesByBasket.get(user);
		
		return rentables == null ? new LinkedList<>() : rentables;
	}
	
	public void empty(String user) {
		rentablesByBasket.computeIfPresent(user, (u, b) -> new LinkedList<>());
	}
	
	public void add(String user, Buyable buyable) {
		rentablesByBasket.computeIfPresent(user, (u, b) -> { b.add(buyable); return b; });
	}
	
	public void delete(String user, String buyable) {
		rentablesByBasket.computeIfPresent(user, (u, b) -> { b.removeIf(rtble -> rtble.getId().equals(buyable)); return b; });
	}
}
