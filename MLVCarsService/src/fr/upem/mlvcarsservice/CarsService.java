package fr.upem.mlvcarsservice;

import java.net.MalformedURLException;
import java.nio.file.Paths;
import java.rmi.RemoteException;
import java.util.List;
import java.util.UUID;

import javax.jws.WebMethod;
import javax.jws.WebService;

import fr.upem.mlvcars.controllers.rentables.Buyable;
import fr.upem.mlvcars.controllers.rentables.BuyingResponse;
import fr.upem.mlvcars.controllers.users.ConnectionResponse;
import fr.upem.mlvcars.data.DataAccess;
import fr.upem.user.AuthenticationTicket;

@WebService
public class CarsService {
	private final DataAccess data;
	private final Baskets baskets = Baskets.getInstance();
	
	public CarsService() throws IllegalStateException, MalformedURLException {
		String path = "C:/Users/Donatien/Documents/Projets/Informatique/REST/Final product/";
		
		String codebase = path + "Shared.jar";
		String policy = path + "sec.policy";
		
		System.out.println(codebase);
		System.out.println(policy);
		
		data = DataAccess.getInstance("localhost", 8089, Paths.get(codebase), Paths.get(policy));
	}
	
	@WebMethod
	public String connect(String login, String password) {
		try {
			ConnectionResponse response = data.getUsersController().connect(login, password);
			
			if (response.isSuccessful()) {
				baskets.create(response.getTicket().toString());
				
				return response.getTicket().toString();
			}
			
			return "";
		} catch (RemoteException e) {
			return "";
		}
	}
	
	@WebMethod
	public Buyable[] getBuyables(String ticket) {
		try {
			List<Buyable> buyables = data.getBuyablesController().getBuyableRentables(new AuthenticationTicket(ticket));

			if (buyables.size() > 0) {
				Buyable[] data = new Buyable[buyables.size()];
				int i = 0;
				
				for (Buyable b : buyables) {
					data[i] = b;
					i++;
				}
				
				return data;
			}
			
			return null;
		} catch (Exception e) {
			return null;
		}
	}
	
	@WebMethod
	public Buyable[] getBasket(String ticket) {
		try {
			List<Buyable> basket = baskets.get(ticket);
			
			if (basket == null) {
				return null;
			}
			
			if (basket.size() > 0) {
				Buyable[] data = new Buyable[basket.size()];
				int i = 0;
				
				for (Buyable b : basket) {
					data[i] = b;
					i++;
				}
				
				return data;
			}
			
			return null;
		} catch (Exception e) {
			return null;
		}
	}
	
	@WebMethod
	public int buy(String ticket) {
		try {
			List<Buyable> buyables = baskets.get(ticket);
			
			for (Buyable b : buyables) {
				BuyingResponse response = data.getBuyablesController().buy(new AuthenticationTicket(ticket), UUID.fromString(b.getId()));
				
				if (response != BuyingResponse.Success) {
					return 1;
				}
			}
			
			return 0;
		} catch (Exception e) {
			return 1;
		}
	}
	
	@WebMethod
	public void addToBasket(String ticket, String rentable) {
		try {
			Buyable buyable = data.getBuyablesController().getBuyable(new AuthenticationTicket(ticket), UUID.fromString(rentable));
			baskets.add(ticket, buyable);
		} catch (Exception e) {
			// DO NOTHING
		}
	}
	
	@WebMethod
	public void empty(String ticket) {
		try {
			baskets.empty(ticket);
		} catch (Exception e) {
			// DO NOTHING
		}
	}
}
