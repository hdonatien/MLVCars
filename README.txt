=== Informations � lire attentivement ===

BasicClient : un client terminal pour le service de location RMI
Client : un client graphique obsol�te (abandonn� par manque de temps et pas maintenu � jour)
Commons : une base de code de gestion commun � tous les projets (r�utilisable dans le futur)
Final product : les diff�rents Jar ex�cutables et le fichier sec.policy
MLVBankService : le service de banque
MLVBankServiceClient : un client automatiquement cr�� pour le service bancaire (utilisable via l'interface d'explorateur de service d'Eclipse)
MLVCarsService : le service d'achat de v�hicules
MLVCarsServiceClient : un client pour le service d'achat de voitures (contient un Main proposant une interface terminal)
Server : le serveur central RMI, qui joue aussi le r�le de base de donn�es
Shared : les interfaces et classes partag�es par le serveur et les clients RMI

=== Lancer ===

1. D�marrer Server.jar
2. D�marrer les web services sur le m�me serveur Tomcat (v8.5) port 8080 (via Eclipse apr�s import de l'ensemble des projets du r�pertoire racine dans le workspace)
3. D�marrer Client.jar avec les arguments suivants :
	localhost
	8089
	[chemin absolu vers Shared.jar]
	[chemin absolu vers sec.policy]

=== Jeux de donn�es ===

Des donn�es de test sont disponibles d�s le lancement :
- Trois comptes de connexion :
	login = student, password = mdp
	login = teacher, password = mdp
	login = out, password = mdp
	/!\ Avec le compte out, il n'est pas possible d'effectuer une quelconque action via BasicClient (service r�serv� aux profs et �l�ves)
	    -> Utiliser le client webservice � la place
- Les trois comptes sont li�s � leur compte bancaire dans diff�rentes devises (automatiquement cr��s par le service de banque)
- Deux v�hicules sont cr��s �galement, dont l'un poss�de des commentaires, a �t� ajout� depuis 4 ans et a �t� lou� 1 fois

=== Structures principales du design et donn�es ===
- Managers : gestionnaires de donn�es (equiv. base de donn�es) -> UsersManager, RentsManager, ...
- Controllers : interfaces publiques de manipulation du service (actions limit�es et contr�l�es), acc�s aux managers en interne, de mani�re transparente

=== Limites ===
- Pas d'impl�mentation de la file d'attente prioritaire demand�e dans le sujet
- Pas d'interface graphique finalis�e (client obsol�te JavaFX disponible) mais une interface terminal travaill�e et color�e sur le client RMI du service de location (BasicClient)
- Lacunes dans la gestion m�tier suppl�mentaire (r�gles m�tier pour certaines fonctionnalit�s sautent certaines v�rifications pour plus de flexibilit� et de rapidit� de d�veloppement, notamment la v�rification de propri�t� d'une location pour son annulation par un utilisateur donn�, par exemple, etc...)

=== Notes ===
Choix du stockage de prix en EUR dans la base de donn�es
-> L'affichage chez l'utilisateur est d�termin� par la devise g�r�e par son compte bancaire, r�cup�r�e automatiquement par les composants mis en cause

=== Le case du service d'achats ===
Il est connect� en RMI au serveur central.
Il manipule pour cela un DataAccess, singleton r�cup�rant automatiquement les diff�rents controllers mis � disposition par le serveur via RMI.