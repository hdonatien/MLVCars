package fr.upem.mlvbank;

public class TransactionResult {
	public static final int Success = 0;
	public static final int UnknownAccount = 1;
	public static final int NotEnoughFunds = 2;
	public static final int InvalidRequest = 3;
	public static final int InternalError = 4;
}
