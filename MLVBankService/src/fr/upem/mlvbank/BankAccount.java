package fr.upem.mlvbank;

import java.util.Currency;
import java.util.Objects;

public class BankAccount {
	private final long number;
	private long balance;
	private final Currency currency;
	
	public BankAccount(long number, Currency currency) {
		this.number = Objects.requireNonNull(number, "Invalid account number.");
		this.balance = 0L;
		this.currency = Objects.requireNonNull(currency, "Invalid currency.");
	}
	
	public long getNumber() {
		return number;
	}
	
	public long getBalance() {
		return balance;
	}
	
	public Currency getCurrency() {
		return currency;
	}
	
	public void debit(long amount) {
		balance -= amount;
	}
	
	public void deposit(long amount) {
		balance += amount;
	}
}
