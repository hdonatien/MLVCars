package fr.upem.mlvbank;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.Calendar;
import java.util.Optional;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.xml.rpc.ServiceException;

import org.tempuri.ConverterLocator;
import org.tempuri.ConverterSoap;

@WebService
public class BankService {
	private final Bank bank = Bank.getInstance();
	
	@WebMethod
	public int transact(Transaction transaction) {
		return bank.acceptTransaction(transaction);
	}
	
	@WebMethod
	public BankAccountSnapShot getSnapShot(long number) {
		try {
			Optional<BankAccount> account = bank.getAccount(number);
			
			if (!account.isPresent()) {
				return new BankAccountSnapShot();
			}
			
			return BankAccountSnapShot.fromAccount(account.get());
		} catch (IllegalArgumentException e) {
			return new BankAccountSnapShot();
		}
	}
	
	@WebMethod
	public long convert(String currencyFrom, String currencyTo, long amount) {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.DATE, -1);
		
		try {
			ConverterSoap service = new ConverterLocator().getConverterSoap();
			BigDecimal rate = service.getConversionRate(currencyFrom, currencyTo, calendar);
			
			return rate.multiply(BigDecimal.valueOf(amount)).longValue();
		} catch (RemoteException | ServiceException e) {
			return -amount;
		}
	}
}
