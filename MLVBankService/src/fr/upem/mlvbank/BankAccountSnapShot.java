package fr.upem.mlvbank;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlType;

@XmlType(name = "BankAccountSnapShot")
public class BankAccountSnapShot implements Serializable {
	private static final long serialVersionUID = -6434628495524256496L;

	private long number;
	private long balance;
	private String currency;
	
	public BankAccountSnapShot() {
		
	}
	
	public BankAccountSnapShot(long number, long balance, String currency) {
		this.number = number;
		this.balance = balance;
		this.currency = currency;
	}
	
	public long getNumber() {
		return number;
	}
	
	public void setNumber(long number) {
		this.number = number;
	}
	
	public long getBalance() {
		return balance;
	}
	
	public void setBalance(long balance) {
		this.balance = balance;
	}
	
	public String getCurrency() {
		return currency;
	}
	
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	
	public static BankAccountSnapShot fromAccount(BankAccount account) {
		return new BankAccountSnapShot(account.getNumber(), account.getBalance(), account.getCurrency().getCurrencyCode());
	}
}
