package fr.upem.mlvbank;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Currency;
import java.util.Objects;
import java.util.Optional;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.tempuri.ConverterLocator;
import org.tempuri.ConverterSoap;

public class Bank {
	private static Bank bank;
	private static final Object monitor = new Object();
	
	public static Bank getInstance() {
		synchronized (monitor) {
			if (bank == null) {
				bank = new Bank();
			}
			
			return bank;
		}
	}
	
	private final Random randomizer = new Random(System.currentTimeMillis());
	
	private final ConcurrentMap<Long, BankAccount> accounts = new ConcurrentHashMap<>();
	
	private Bank() {
		// Jeu de données de simulation
		
		BankAccount a1 = new BankAccount(1L, Currency.getInstance("USD"));
		BankAccount a2 = new BankAccount(2L, Currency.getInstance("GBP"));
		BankAccount a3 = new BankAccount(3L, Currency.getInstance("INR"));
		
		accounts.put(a1.getNumber(), a1);
		accounts.put(a2.getNumber(), a2);
		accounts.put(a3.getNumber(), a3);
	}
	
	public Long createAccount(Currency currency) {
		Objects.requireNonNull(currency, "Invalid currency.");
		
		Long number = randomizer.nextLong();
		BankAccount account = new BankAccount(number, currency);
		
		accounts.put(number, account);
		
		return number;
	}
	
	public Optional<BankAccount> getAccount(long number) throws IllegalArgumentException {
		return Optional.<BankAccount>ofNullable(accounts.get(number));
	}

	public int acceptTransaction(Transaction transaction) {
		BankAccount account = accounts.get(transaction.getAccountNumber());
		
		if (account == null) {
			return TransactionResult.UnknownAccount;
		}
		
		try {
			Currency currency = Currency.getInstance(transaction.getCurrency());
			Currency accountCurrency = account.getCurrency();
			Calendar rateDate = Calendar.getInstance();
			rateDate.set(Calendar.DATE, -1);
			
			ConverterSoap service = new ConverterLocator().getConverterSoap();
			BigDecimal rate = service.getConversionRate(currency.getCurrencyCode(), accountCurrency.getCurrencyCode(), rateDate);
			long trueAmount = (rate.multiply(BigDecimal.valueOf(transaction.getAmount()))).longValue();
			
			switch (transaction.getType()) {
			case TransactionType.Debit :
				return acceptDebit(account, trueAmount);
			case TransactionType.Deposit :
				return acceptDeposit(account, trueAmount);
			default :
				return TransactionResult.InvalidRequest;	
			}
		} catch (Exception e) {
			return TransactionResult.InternalError;
		}
	}

	private int acceptDebit(BankAccount account, long trueAmount) {
		if (trueAmount < 0) {
			return TransactionResult.InvalidRequest;
		}
		
		if (account.getBalance() < trueAmount) {
			return TransactionResult.NotEnoughFunds;
		}
		
		account.debit(trueAmount);
		
		return TransactionResult.Success;
	}

	private int acceptDeposit(BankAccount account, long trueAmount) {
		if (trueAmount < 0) {
			return TransactionResult.InvalidRequest;
		}
		
		account.deposit(trueAmount);
		
		return TransactionResult.Success;
	}
}
