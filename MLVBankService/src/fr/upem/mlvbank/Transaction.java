package fr.upem.mlvbank;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlType;

@XmlType(name = "Transaction")
public class Transaction implements Serializable {
	private static final long serialVersionUID = 9177009076530805642L;
	
	private long accountNumber;
	private int type;
	private String currency;
	private long amount;
	
	public Transaction() {
		
	}
	
	public Transaction(long accountNumber, int type, String currency, long amount) {
		this.accountNumber = accountNumber;
		this.type = type;
		this.currency = currency;
		this.amount = amount;
	}
	
	public long getAccountNumber() {
		return accountNumber;
	}
	
	public void setAccountNumber(long accountNumber) {
		this.accountNumber = accountNumber;
	}
	
	public int getType() {
		return type;
	}
	
	public void setType(int type) {
		this.type = type;
	}
	
	public String getCurrency() {
		return currency;
	}
	
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	
	public long getAmount() {
		return amount;
	}
	
	public void setAmount(long amount) {
		this.amount = amount;
	}
}
