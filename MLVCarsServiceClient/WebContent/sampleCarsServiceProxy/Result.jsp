<%@page contentType="text/html;charset=UTF-8"%>
<% request.setCharacterEncoding("UTF-8"); %>
<HTML>
<HEAD>
<TITLE>Result</TITLE>
</HEAD>
<BODY>
<H1>Result</H1>

<jsp:useBean id="sampleCarsServiceProxyid" scope="session" class="fr.upem.mlvcarsservice.CarsServiceProxy" />
<%
if (request.getParameter("endpoint") != null && request.getParameter("endpoint").length() > 0)
sampleCarsServiceProxyid.setEndpoint(request.getParameter("endpoint"));
%>

<%
String method = request.getParameter("method");
int methodID = 0;
if (method == null) methodID = -1;

if(methodID != -1) methodID = Integer.parseInt(method);
boolean gotMethod = false;

try {
switch (methodID){ 
case 2:
        gotMethod = true;
        java.lang.String getEndpoint2mtemp = sampleCarsServiceProxyid.getEndpoint();
if(getEndpoint2mtemp == null){
%>
<%=getEndpoint2mtemp %>
<%
}else{
        String tempResultreturnp3 = org.eclipse.jst.ws.util.JspUtils.markup(String.valueOf(getEndpoint2mtemp));
        %>
        <%= tempResultreturnp3 %>
        <%
}
break;
case 5:
        gotMethod = true;
        String endpoint_0id=  request.getParameter("endpoint8");
            java.lang.String endpoint_0idTemp = null;
        if(!endpoint_0id.equals("")){
         endpoint_0idTemp  = endpoint_0id;
        }
        sampleCarsServiceProxyid.setEndpoint(endpoint_0idTemp);
break;
case 10:
        gotMethod = true;
        fr.upem.mlvcarsservice.CarsService getCarsService10mtemp = sampleCarsServiceProxyid.getCarsService();
if(getCarsService10mtemp == null){
%>
<%=getCarsService10mtemp %>
<%
}else{
        if(getCarsService10mtemp!= null){
        String tempreturnp11 = getCarsService10mtemp.toString();
        %>
        <%=tempreturnp11%>
        <%
        }}
break;
case 13:
        gotMethod = true;
        String login_1id=  request.getParameter("login16");
            java.lang.String login_1idTemp = null;
        if(!login_1id.equals("")){
         login_1idTemp  = login_1id;
        }
        String password_2id=  request.getParameter("password18");
            java.lang.String password_2idTemp = null;
        if(!password_2id.equals("")){
         password_2idTemp  = password_2id;
        }
        java.lang.String connect13mtemp = sampleCarsServiceProxyid.connect(login_1idTemp,password_2idTemp);
if(connect13mtemp == null){
%>
<%=connect13mtemp %>
<%
}else{
        String tempResultreturnp14 = org.eclipse.jst.ws.util.JspUtils.markup(String.valueOf(connect13mtemp));
        %>
        <%= tempResultreturnp14 %>
        <%
}
break;
case 20:
        gotMethod = true;
        String ticket_3id=  request.getParameter("ticket23");
            java.lang.String ticket_3idTemp = null;
        if(!ticket_3id.equals("")){
         ticket_3idTemp  = ticket_3id;
        }
        fr.upem.mlvcars.controllers.rentables.Buyable[] getBasket20mtemp = sampleCarsServiceProxyid.getBasket(ticket_3idTemp);
if(getBasket20mtemp == null){
%>
<%=getBasket20mtemp %>
<%
}else{
        String tempreturnp21 = null;
        if(getBasket20mtemp != null){
        java.util.List listreturnp21= java.util.Arrays.asList(getBasket20mtemp);
        tempreturnp21 = listreturnp21.toString();
        }
        %>
        <%=tempreturnp21%>
        <%
}
break;
case 25:
        gotMethod = true;
        String ticket_4id=  request.getParameter("ticket28");
            java.lang.String ticket_4idTemp = null;
        if(!ticket_4id.equals("")){
         ticket_4idTemp  = ticket_4id;
        }
        int buy25mtemp = sampleCarsServiceProxyid.buy(ticket_4idTemp);
        String tempResultreturnp26 = org.eclipse.jst.ws.util.JspUtils.markup(String.valueOf(buy25mtemp));
        %>
        <%= tempResultreturnp26 %>
        <%
break;
case 30:
        gotMethod = true;
        String ticket_5id=  request.getParameter("ticket33");
            java.lang.String ticket_5idTemp = null;
        if(!ticket_5id.equals("")){
         ticket_5idTemp  = ticket_5id;
        }
        String rentable_6id=  request.getParameter("rentable35");
            java.lang.String rentable_6idTemp = null;
        if(!rentable_6id.equals("")){
         rentable_6idTemp  = rentable_6id;
        }
        sampleCarsServiceProxyid.addToBasket(ticket_5idTemp,rentable_6idTemp);
break;
case 37:
        gotMethod = true;
        String ticket_7id=  request.getParameter("ticket40");
            java.lang.String ticket_7idTemp = null;
        if(!ticket_7id.equals("")){
         ticket_7idTemp  = ticket_7id;
        }
        fr.upem.mlvcars.controllers.rentables.Buyable[] getBuyables37mtemp = sampleCarsServiceProxyid.getBuyables(ticket_7idTemp);
if(getBuyables37mtemp == null){
%>
<%=getBuyables37mtemp %>
<%
}else{
        String tempreturnp38 = null;
        if(getBuyables37mtemp != null){
        java.util.List listreturnp38= java.util.Arrays.asList(getBuyables37mtemp);
        tempreturnp38 = listreturnp38.toString();
        }
        %>
        <%=tempreturnp38%>
        <%
}
break;
case 42:
        gotMethod = true;
        String ticket_8id=  request.getParameter("ticket45");
            java.lang.String ticket_8idTemp = null;
        if(!ticket_8id.equals("")){
         ticket_8idTemp  = ticket_8id;
        }
        sampleCarsServiceProxyid.empty(ticket_8idTemp);
break;
}
} catch (Exception e) { 
%>
Exception: <%= org.eclipse.jst.ws.util.JspUtils.markup(e.toString()) %>
Message: <%= org.eclipse.jst.ws.util.JspUtils.markup(e.getMessage()) %>
<%
return;
}
if(!gotMethod){
%>
result: N/A
<%
}
%>
</BODY>
</HTML>