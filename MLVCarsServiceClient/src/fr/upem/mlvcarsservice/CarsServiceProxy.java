package fr.upem.mlvcarsservice;

public class CarsServiceProxy implements fr.upem.mlvcarsservice.CarsService {
  private String _endpoint = null;
  private fr.upem.mlvcarsservice.CarsService carsService = null;
  
  public CarsServiceProxy() {
    _initCarsServiceProxy();
  }
  
  public CarsServiceProxy(String endpoint) {
    _endpoint = endpoint;
    _initCarsServiceProxy();
  }
  
  private void _initCarsServiceProxy() {
    try {
      carsService = (new fr.upem.mlvcarsservice.CarsServiceServiceLocator()).getCarsService();
      if (carsService != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)carsService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)carsService)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (carsService != null)
      ((javax.xml.rpc.Stub)carsService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public fr.upem.mlvcarsservice.CarsService getCarsService() {
    if (carsService == null)
      _initCarsServiceProxy();
    return carsService;
  }
  
  public java.lang.String connect(java.lang.String login, java.lang.String password) throws java.rmi.RemoteException{
    if (carsService == null)
      _initCarsServiceProxy();
    return carsService.connect(login, password);
  }
  
  public fr.upem.mlvcars.controllers.rentables.Buyable[] getBasket(java.lang.String ticket) throws java.rmi.RemoteException{
    if (carsService == null)
      _initCarsServiceProxy();
    return carsService.getBasket(ticket);
  }
  
  public int buy(java.lang.String ticket) throws java.rmi.RemoteException{
    if (carsService == null)
      _initCarsServiceProxy();
    return carsService.buy(ticket);
  }
  
  public void addToBasket(java.lang.String ticket, java.lang.String rentable) throws java.rmi.RemoteException{
    if (carsService == null)
      _initCarsServiceProxy();
    carsService.addToBasket(ticket, rentable);
  }
  
  public fr.upem.mlvcars.controllers.rentables.Buyable[] getBuyables(java.lang.String ticket) throws java.rmi.RemoteException{
    if (carsService == null)
      _initCarsServiceProxy();
    return carsService.getBuyables(ticket);
  }
  
  public void empty(java.lang.String ticket) throws java.rmi.RemoteException{
    if (carsService == null)
      _initCarsServiceProxy();
    carsService.empty(ticket);
  }
  
  
}