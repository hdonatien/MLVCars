/**
 * CarsServiceServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package fr.upem.mlvcarsservice;

public class CarsServiceServiceLocator extends org.apache.axis.client.Service implements fr.upem.mlvcarsservice.CarsServiceService {

    public CarsServiceServiceLocator() {
    }


    public CarsServiceServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public CarsServiceServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for CarsService
    private java.lang.String CarsService_address = "http://localhost:9101/MLVCarsService/services/CarsService";

    public java.lang.String getCarsServiceAddress() {
        return CarsService_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String CarsServiceWSDDServiceName = "CarsService";

    public java.lang.String getCarsServiceWSDDServiceName() {
        return CarsServiceWSDDServiceName;
    }

    public void setCarsServiceWSDDServiceName(java.lang.String name) {
        CarsServiceWSDDServiceName = name;
    }

    public fr.upem.mlvcarsservice.CarsService getCarsService() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(CarsService_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getCarsService(endpoint);
    }

    public fr.upem.mlvcarsservice.CarsService getCarsService(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            fr.upem.mlvcarsservice.CarsServiceSoapBindingStub _stub = new fr.upem.mlvcarsservice.CarsServiceSoapBindingStub(portAddress, this);
            _stub.setPortName(getCarsServiceWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setCarsServiceEndpointAddress(java.lang.String address) {
        CarsService_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (fr.upem.mlvcarsservice.CarsService.class.isAssignableFrom(serviceEndpointInterface)) {
                fr.upem.mlvcarsservice.CarsServiceSoapBindingStub _stub = new fr.upem.mlvcarsservice.CarsServiceSoapBindingStub(new java.net.URL(CarsService_address), this);
                _stub.setPortName(getCarsServiceWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("CarsService".equals(inputPortName)) {
            return getCarsService();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://mlvcarsservice.upem.fr", "CarsServiceService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://mlvcarsservice.upem.fr", "CarsService"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("CarsService".equals(portName)) {
            setCarsServiceEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
