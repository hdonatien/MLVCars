/**
 * CarsService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package fr.upem.mlvcarsservice;

public interface CarsService extends java.rmi.Remote {
    public java.lang.String connect(java.lang.String login, java.lang.String password) throws java.rmi.RemoteException;
    public void empty(java.lang.String ticket) throws java.rmi.RemoteException;
    public fr.upem.mlvcars.controllers.rentables.Buyable[] getBasket(java.lang.String ticket) throws java.rmi.RemoteException;
    public int buy(java.lang.String ticket) throws java.rmi.RemoteException;
    public void addToBasket(java.lang.String ticket, java.lang.String rentable) throws java.rmi.RemoteException;
    public fr.upem.mlvcars.controllers.rentables.Buyable[] getBuyables(java.lang.String ticket) throws java.rmi.RemoteException;
}
