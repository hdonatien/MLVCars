/**
 * CarsServiceService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package fr.upem.mlvcarsservice;

public interface CarsServiceService extends javax.xml.rpc.Service {
    public java.lang.String getCarsServiceAddress();

    public fr.upem.mlvcarsservice.CarsService getCarsService() throws javax.xml.rpc.ServiceException;

    public fr.upem.mlvcarsservice.CarsService getCarsService(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
