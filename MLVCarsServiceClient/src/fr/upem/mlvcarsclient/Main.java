package fr.upem.mlvcarsclient;

import java.rmi.RemoteException;
import java.util.Scanner;

import fr.upem.mlvcars.controllers.rentables.Buyable;
import fr.upem.mlvcarsservice.CarsService;
import fr.upem.mlvcarsservice.CarsServiceServiceLocator;

public class Main {
	private final CarsService service;
	private String ticket;
	
	public Main(CarsService service) {
		this.service = service;
	}
	
	public void start() throws Exception {
		try (Scanner input = new Scanner(System.in)) {
			System.out.print("Login : ");
			String login = input.nextLine();
	
			System.out.print("Mot de passe : ");
			String password = input.nextLine();
			
			System.out.println();
			
			if (connect(login, password)) {
				System.out.println("Vous êtes connecté(e).\n");
				menu(input);
			} else {
				System.out.println("Erreur de connexion.");
			}
		}
	}
	
	private boolean connect(String login, String password) throws RemoteException {
		String ticket = service.connect(login, password);
		this.ticket = ticket;
		return !ticket.isEmpty();
	}
	
	private void menu(Scanner input) throws RemoteException {
		int choice;
		
		do {
			System.out.println("=== Menu ===\n");
			System.out.println("1. Mon panier.");
			System.out.println("2. Voir les articles.");
			System.out.println("3. Quitter.");
			System.out.print("> ");
			
			choice = Integer.parseInt(input.nextLine());
			
			System.out.println();
			
			switch (choice) {
			case 1 :
				basket(input);
				break;
			case 2 :
				articles(input);
				break;
			case 3 :
				return;
			default :
				break;
			}
		} while (choice != 3);
	}
	
	private void basket(Scanner input) throws RemoteException {
		int choice;
		
		do {
			System.out.println("=== Mon panier ===\n");
			
			Buyable[] buyables = service.getBasket(ticket);
			long price = 0;
			
			for (int i = 1; i <= buyables.length; i++) {
				printBuyable(i, buyables[i - 1]);
				price += buyables[i - 1].getPrice();
			}
			
			System.out.println();
			System.out.println("Prix total : " + price + " " + (buyables.length > 0 ? buyables[0].getCurrency() : "aucun article."));
	
			System.out.println();
			System.out.println("1. Finaliser.");
			System.out.println("2. Vider.");
			System.out.println("3. Retour.");
			
			do {
				System.out.print("> ");
				choice = Integer.parseInt(input.nextLine());
			} while (choice != 1 && choice != 2 && choice != 3);
			
			switch (choice) {
			case 1 :
				validateBasket();
				break;
			case 2 :
				emptyBasket();
				break;
			default :
				break;
			}
		} while (choice != 3);
	}

	private void emptyBasket() throws RemoteException {
		service.empty(ticket);
	}

	private void validateBasket() throws RemoteException {
		if (service.buy(ticket) == 0) {
			System.out.println("Achat effectué avec succès. Félicitations !\n");
		} else {
			System.out.println("Achat refusé.\n");
		}
	}

	private void printBuyable(int index, Buyable b) {
		System.out.println(index + ". " + b.getDescription() + " [" + (b.getPrice() / 100.) + " " + b.getCurrency() + "]");
	}

	private void articles(Scanner input) throws RemoteException {
		int choice;
		
		do {
			System.out.println("=== Articles disponibles ===\n");
			
			Buyable[] buyables = service.getBuyables(ticket);
			
			for (int i = 1; i <= buyables.length; i++) {
				printBuyable(i, buyables[i - 1]);
			}
	
			System.out.println();
			System.out.print("Ajouter l'article (0 = annuler) : ");
			choice = Integer.parseInt(input.nextLine());
			
			if (choice != 0) {
				service.addToBasket(ticket, buyables[choice].getId());
			}
		} while (choice != 0);
	}

	public static void main(String[] args) {
		try {
			CarsService service = new CarsServiceServiceLocator().getCarsService();
			new Main(service).start();
		} catch (Exception e) {
			System.out.println("Erreur interne.");
		}
	}
}
