package fr.upem.mlvcars.data;

import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Path;
import java.rmi.Naming;

import fr.upem.mlvcars.controllers.rentables.IBuyablesController;
import fr.upem.mlvcars.controllers.rentables.IRentablesController;
import fr.upem.mlvcars.controllers.rents.IRentsController;
import fr.upem.mlvcars.controllers.users.IUsersController;

public class DataAccess {
	public static class DataAccessException extends Exception {
		private static final long serialVersionUID = 3726478165596699899L;
		
		public DataAccessException(String message) {
			super(message);
		}
	}
	
	private final IUsersController users;
	private final IRentablesController rentables;
	private final IRentsController rents;
	private final IBuyablesController buyables;

	private static DataAccess data;
	private static Object monitor = new Object();
	
	public static DataAccess getInstance(String host, int port, Path shared, Path policy) throws IllegalStateException, MalformedURLException {
		synchronized (monitor) {
			if (data == null) {
				data = new DataAccess(host, port, shared.toFile().toURI().toURL(), policy.toFile().toURI().toURL());
			}
			
			return data;
		}
	}
	
	private DataAccess(String host, int port, URL shared, URL policy) throws IllegalStateException {
		try {
			System.setProperty("java.rmi.server.codebase", shared.toString());
			System.setProperty("java.security.policy", policy.toString());
	
			System.setSecurityManager(new SecurityManager());
	
			users = (IUsersController) Naming.lookup("rmi://" + host + ":" + port + "/users");
			rentables = (IRentablesController) Naming.lookup("rmi://" + host + ":" + port + "/rentables");
			rents = (IRentsController) Naming.lookup("rmi://" + host + ":" + port + "/rents");
			buyables = (IBuyablesController) Naming.lookup("rmi://" + host + ":" + port + "/buyables");
		} catch (Exception e) {
			throw new IllegalStateException("Error while connecting to data center: " + e.getMessage());
		}
	}
	
	public IUsersController getUsersController() {
		return users;
	}
	
	public IRentsController getRentsController() {
		return rents;
	}
	
	public IRentablesController getRentablesController() {
		return rentables;
	}
	
	public IBuyablesController getBuyablesController() {
		return buyables;
	}
}
