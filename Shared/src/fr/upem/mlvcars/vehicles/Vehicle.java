package fr.upem.mlvcars.vehicles;

import java.time.Year;
import java.util.Objects;

import fr.upem.contract.Contract;
import fr.upem.mlvcars.rentables.RentableType;
import fr.upem.rent.Rentable;

public abstract class Vehicle implements Rentable<RentableType> {
	private static final long serialVersionUID = -7827035646088261693L;
	
	private final String constructor;
	private final String model;
	private final int year;
	private final String matriculation;

	public Vehicle(String constructor, String model, int year, String matriculation) throws IllegalArgumentException {
		this.constructor = Objects.requireNonNull(constructor, "Invalid constructor.");
		this.model = Objects.requireNonNull(model, "Invalid model.");
		this.year = Contract.checkThat(year, y -> y >= 1900 || y <= Year.now().getValue(), "Invalid year.");
		this.matriculation = Objects.requireNonNull(matriculation, "Invalid matriculation.");
	}

	public String getConstructor() {
		return constructor;
	}

	public String getModel() {
		return model;
	}

	public int getYear() {
		return year;
	}
	
	public String getMatriculation() {
		return matriculation;
	}

	@Override
	public String toString() {
		return constructor + " - " + model + " (" + year + ")";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;

		result = prime * result + constructor.hashCode();
		result = prime * result + model.hashCode();
		result = prime * result + year;
		result = prime * result + matriculation.hashCode();

		return result;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}

		if (!(o instanceof Vehicle)) {
			return false;
		}

		Vehicle v = (Vehicle) o;

		return constructor.equals(v.constructor)
		&& model.equals(v.model)
		&& year == v.year
		&& matriculation.equals(v.matriculation);
	}
}
