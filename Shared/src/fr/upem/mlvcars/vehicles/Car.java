package fr.upem.mlvcars.vehicles;

import fr.upem.contract.Contract;
import fr.upem.mlvcars.rentables.RentableType;
import fr.upem.rent.Rentable;

public class Car extends Vehicle {
	private static final long serialVersionUID = -4203008519035579876L;
	
	private final int doors;

	public Car(String constructor, String model, int year, String matriculation, int doors) throws IllegalArgumentException {
		super(constructor, model, year, matriculation);

		this.doors = Contract.checkThat(doors, d -> d > 0, "Invalid number of doors.");
	}

	public int getDoors() {
		return doors;
	}

	@Override
	public String toString() {
		return super.toString() + " - " + doors + " portes";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;

		result = prime * result + super.hashCode();
		result = prime * result + doors;

		return result;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}

		if (!(o instanceof Car)) {
			return false;
		}

		Car c = (Car) o;

		return super.equals(o) && doors == c.doors;
	}

	@Override
	public String getDescription() {
		return this.toString();
	}

	@Override
	public String getTitle() {
		return super.toString();
	}

	@Override
	public RentableType getType() {
		return RentableType.Car;
	}

	@Override
	public Rentable<RentableType> copy() {
		return new Car(this.getConstructor(), this.getModel(), this.getYear(), this.getMatriculation(), doors);
	}
}
