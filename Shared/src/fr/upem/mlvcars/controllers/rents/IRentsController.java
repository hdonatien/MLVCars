package fr.upem.mlvcars.controllers.rents;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.UUID;

import fr.upem.rate.Rate;
import fr.upem.rent.RentsManager.Renting;
import fr.upem.store.Entity;
import fr.upem.user.AuthenticationTicket;

public interface IRentsController extends Remote {
	List<Entity<Renting>> getRentsForUser(AuthenticationTicket ticket) throws IllegalStateException, RemoteException;
	RentResponse takeRent(AuthenticationTicket ticket, UUID rentable, ZonedDateTime start, ZonedDateTime end) throws IllegalStateException, RemoteException;
	CancelResponse cancelRent(AuthenticationTicket ticket, UUID rent) throws IllegalStateException, RemoteException;
	FinishResponse finishRent(AuthenticationTicket ticket, UUID rent) throws IllegalStateException, RemoteException;
	FinishResponse finishRent(AuthenticationTicket ticket, UUID rent, Rate rate) throws IllegalStateException, RemoteException;
	ExtensionResponse extendRent(AuthenticationTicket ticket, UUID rent, ZonedDateTime newEnd) throws IllegalStateException, RemoteException;
}
