package fr.upem.mlvcars.controllers.rents;

public enum CancelResponse {
	Success,
	ErrorInvalidRent,
	ErrorRentIsActive,
	ErrorAlreadyCancelledRent,
	ErrorAlreadyFinishedRent,
	ErrorInternal
}
