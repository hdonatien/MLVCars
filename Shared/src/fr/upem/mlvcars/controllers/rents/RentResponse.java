package fr.upem.mlvcars.controllers.rents;

public enum RentResponse {
	Success,
	ErrorInvalidRentable,
	ErrorInvalidDates,
	ErrorNotAvailableRentable,
	ErrorNotAvailableRentableForPeriod,
	ErrorInternal,
	ErrorPaymentFailed
}
