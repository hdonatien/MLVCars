package fr.upem.mlvcars.controllers.rents;

public enum ExtensionResponse {
	Success,
	ErrorInvalidRent,
	ErrorInvalidNewEndDate,
	ErrorRentIsInThePast,
	ErrorCancelledRent,
	ErrorFinishedRent,
	ErrorNotAvailablePeriod,
	ErrorPaymentFailed,
	ErrorInternal
}
