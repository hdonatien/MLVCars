package fr.upem.mlvcars.controllers.users;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.Objects;

import fr.upem.contract.Contract;
import fr.upem.mlvcars.users.UserType;
import fr.upem.store.Entity;
import fr.upem.user.AuthenticationTicket;
import fr.upem.user.User;

public class ConnectionResponse implements Serializable {
	private static final long serialVersionUID = 2729174613991797917L;

	public static enum Code {
		Success,
		ErrorInvalidLoginAndOrPassword,
		NetworkFailure,		
	}

	private final AuthenticationTicket ticket;
	private final Entity<User<UserType>> user;
	private final Code code;

	public ConnectionResponse(AuthenticationTicket ticket, Entity<User<UserType>> user) {
		super();

		this.ticket = Objects.requireNonNull(ticket, "Invalid authentication ticket.");
		this.user = Objects.requireNonNull(user, "Invalid user.");
		this.code = Code.Success;
	}

	public ConnectionResponse(Code code) {
		super();

		Contract.checkThat(code, c -> c != Code.Success, "An empty authentication ticket can't represent a successful connection.");

		this.ticket = null;
		this.user = null;
		this.code = Objects.requireNonNull(code, "Invalid code.");
	}

	public boolean isSuccessful() {
		return code == Code.Success;
	}

	public Code getCode() throws RemoteException {
		return code;
	}

	public AuthenticationTicket getTicket() throws IllegalStateException {
		if (ticket == null) {
			throw new IllegalStateException("Authentication ticket is unavailable.");
		}

		return ticket;
	}

	public Entity<User<UserType>> getUser() throws IllegalStateException {
		if (user == null) {
			throw new IllegalStateException("User is unavailable.");
		}

		return user;
	}
}
