package fr.upem.mlvcars.controllers.users;

import java.rmi.Remote;
import java.rmi.RemoteException;

import fr.upem.user.AuthenticationTicket;

public interface IUsersController extends Remote {
	ConnectionResponse connect(String login, String password) throws RemoteException;
	void disconnect(AuthenticationTicket ticket) throws RemoteException;
	long getUserBankAccount(AuthenticationTicket ticket) throws IllegalStateException, RemoteException;
}