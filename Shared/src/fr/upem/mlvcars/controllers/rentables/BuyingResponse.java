package fr.upem.mlvcars.controllers.rentables;

public enum BuyingResponse {
	Success,
	ErrorRentableNotBuyable,
	ErrorPaymentFailed,
	ErrorInternal
}
