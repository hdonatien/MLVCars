package fr.upem.mlvcars.controllers.rentables;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;
import java.util.UUID;

import fr.upem.user.AuthenticationTicket;

public interface IBuyablesController extends Remote {
	List<Buyable> getBuyableRentables(AuthenticationTicket ticket) throws RemoteException;
	BuyingResponse buy(AuthenticationTicket ticket, UUID buyable) throws IllegalArgumentException, RemoteException;
	Buyable getBuyable(AuthenticationTicket ticket, UUID buyable) throws IllegalArgumentException, RemoteException;
}
