package fr.upem.mlvcars.controllers.rentables;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;
import java.util.UUID;

import fr.upem.mlvcars.rentables.RentableType;
import fr.upem.rate.Rate;
import fr.upem.store.Entity;
import fr.upem.user.AuthenticationTicket;

public interface IRentablesController extends Remote {
	List<RentableData> getRentablesForUser(AuthenticationTicket ticket) throws IllegalStateException, RemoteException;
	List<RentableData> getRentables(AuthenticationTicket ticket) throws IllegalStateException, RemoteException;
	List<RentableData> getFilteredRentables(AuthenticationTicket ticket, RentableType type) throws IllegalStateException, RemoteException;
	void createCar(AuthenticationTicket ticket, String constructor, String model, int year, String matriculation, int doors, long price) throws IllegalStateException, RemoteException;
	void modifyCar(AuthenticationTicket ticket, UUID car, String constructor, String model, int year, String matriculation, int doors, long price) throws IllegalStateException, RemoteException;
	List<Entity<Rate>> getComments(AuthenticationTicket ticket, UUID rentable) throws IllegalStateException, RemoteException;
}
