package fr.upem.mlvcars.controllers.rentables;

import java.io.Serializable;
import java.util.Objects;

import javax.xml.bind.annotation.XmlType;

@XmlType(name="Buyable")
public class Buyable implements Serializable {
	private static final long serialVersionUID = -5665188238752280515L;

	private String id;
	private String description;
	private long price;
	private String currency;
	
	public Buyable() {
		
	}
	
	public Buyable(String id, String description, long price, String currency) {
		this.id = Objects.requireNonNull(id, "Invalid id.");
		this.description = Objects.requireNonNull(description, "Invalid description.");
		this.currency = Objects.requireNonNull(currency, "Invalid currency.");
	}
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		
	}
	
	public long getPrice() {
		return price;
	}
	
	public void setPrice(long price) {
		this.price = price;
	}
	
	public String getCurrency() {
		return currency;
	}
	
	public void setCurrency(String currency) {
		this.currency = currency;
	}
}
