package fr.upem.mlvcars.controllers.rentables;

import java.io.Serializable;
import java.util.Currency;
import java.util.Objects;

import fr.upem.contract.Contract;
import fr.upem.mlvcars.rentables.RentableType;
import fr.upem.rent.Rentable;
import fr.upem.store.Entity;

public class RentableData implements Serializable {
	private static final long serialVersionUID = -1825610217022811247L;
	
	private final Entity<Rentable<RentableType>> rentable;
	private final long price;
	private final Currency currency;
	
	public RentableData(Entity<Rentable<RentableType>> rentable, long price, Currency currency) {
		this.rentable = Objects.requireNonNull(rentable, "Invalid rentable.");
		this.price = Contract.checkThat(price, p -> p >= 0, "Invalid price.");
		this.currency = Objects.requireNonNull(currency, "Invalid currency.");
	}
	
	public Entity<Rentable<RentableType>> getRentable() {
		return rentable;
	}
	
	public long getPrice() {
		return price;
	}
	
	public Currency getCurrency() {
		return currency;
	}
	
	public Buyable toBuyable() {
		return new Buyable(rentable.getId().toString(), rentable.getElement().getDescription(), price, currency.getCurrencyCode());
	}
}
