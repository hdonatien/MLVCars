package fr.upem.mlvcars.rentables;

public enum RentableType {
	Car("Voiture");

	private final String title;

	private RentableType(String title) {
		this.title = title;
	}

	public String getTitle() {
		return title;
	}
}
