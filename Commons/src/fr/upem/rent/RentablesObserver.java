package fr.upem.rent;

import java.util.UUID;

public interface RentablesObserver {
	void rented(UUID rentable);
}
