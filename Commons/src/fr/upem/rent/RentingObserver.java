package fr.upem.rent;

import java.util.UUID;

public interface RentingObserver<T> {
	void rented(UUID rentable);
}
