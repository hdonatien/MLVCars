package fr.upem.rent;

import fr.upem.store.Storable;

public interface Rentable<T extends Enum<T>> extends Storable<Rentable<T>> {
	public T getType();
	public String getTitle();
	public String getDescription();
}
