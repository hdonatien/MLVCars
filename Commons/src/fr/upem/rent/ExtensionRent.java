package fr.upem.rent;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

import fr.upem.contract.Contract;

public class ExtensionRent extends Rent {
	private static final long serialVersionUID = 1733273839230762915L;
	
	private final Rent base;

	public ExtensionRent(Rent base, ZonedDateTime end) {
		super(base.getStart(), end, Type.Extension);

		Objects.requireNonNull(base, "Invalid base rent.");
		Contract.checkThat(end, e -> end.isAfter(base.getEnd()), "Extension rent can't end before base rent.");

		this.base = base;
	}

	public Rent getBaseRent() {
		return base;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;

		result = prime * result + super.hashCode();
		result = prime * result + base.hashCode();

		return result;
	}

	@Override
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}

		if (!(o instanceof ExtensionRent)) {
			return false;
		}

		ExtensionRent rent = (ExtensionRent) o;

		return super.equals(rent) && base.equals(rent.base);
	}

	@Override
	public String toString() {
		return base.toString() + " [extended to " + super.getEnd().format(DateTimeFormatter.RFC_1123_DATE_TIME) + "]";
	}

	@Override
	public Rent copy() {
		return new ExtensionRent(base, this.getEnd());
	}
}
