package fr.upem.rent;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Collectors;

import fr.upem.contract.Contract;
import fr.upem.observable.Observable;
import fr.upem.store.ConcurrentStorage;
import fr.upem.store.Entity;
import fr.upem.store.Storable;
import fr.upem.store.Storage;

public class RentsManager<T extends Enum<T>> extends Observable<RentingObserver<T>> implements IRentsManager<T> {
	public static class Renting implements Storable<Renting> {
		private static final long serialVersionUID = -8541349728782799924L;
		
		private final Rent rent;
		private final UUID user;
		private final UUID rentable;
		private final long price;
		
		public Renting(Rent rent, UUID user, UUID rentable, long price) {
			this.rent = Objects.requireNonNull(rent, "Invalid rent.");
			this.user = Objects.requireNonNull(user, "Invalid user.");
			this.rentable = Objects.requireNonNull(rentable, "Invalid rentable.");
			this.price = Contract.checkThat(price, p -> p >= 0, "Invalid price.");
		}
		
		public Rent getRent() {
			return rent;
		}
		
		public UUID getUser() {
			return user;
		}
		
		public UUID getRentable() {
			return rentable;
		}
		
		public long getPrice() {
			return price;
		}
		
		@Override
		public Renting copy() {
			return new Renting(rent, user, rentable, price);
		}
	}
	
	private final Storage<Renting> rents = new ConcurrentStorage<>();
	
	private final ConcurrentMap<UUID, ZonedDateTime> cancelled = new ConcurrentHashMap<>();
	private final ConcurrentMap<UUID, ZonedDateTime> finished = new ConcurrentHashMap<>();
	private final ConcurrentLinkedQueue<UUID> exceeded = new ConcurrentLinkedQueue<>();
	
	@Override
	public UUID add(Rent rent, UUID user, UUID rentable, long price) throws IllegalArgumentException {
		Objects.requireNonNull(rent, "Invalid rent.");
		Objects.requireNonNull(user, "Invalid user.");
		Objects.requireNonNull(rentable, "Invalid rentable.");
		Contract.checkThat(price, p -> p >= 0, "Invalid price.");
		
		checkAvailability(rent, rentable);
		
		UUID id = rents.insert(new Renting(rent, user, rentable, price));
		this.notify(o -> o.rented(rentable));
		
		return id;
	}
	
	@Override
	public void cancel(UUID rent) throws IllegalArgumentException {
		Objects.requireNonNull(rent, "Invalid rent.");

		Optional<Entity<Renting>> rentData = rents.get(rent);

		if (!rentData.isPresent()) {
			throw new IllegalArgumentException("Unknown rent.");
		}

		Rent theRent = rentData.get().getElement().getRent();
		Contract.checkThat(theRent, r -> !r.hasBegun(), "Rent has already begun.");
		
		Contract.checkThat(cancelled, m -> !m.containsKey(rent), "Rent has already been cancelled.");
		Contract.checkThat(finished, m -> !m.containsKey(rent), "Rent is finished.");
		
		cancelled.put(rent, ZonedDateTime.now());
	}
	
	@Override
	public void declareFinished(UUID rent) throws IllegalArgumentException {
		Objects.requireNonNull(rent, "Invalid rent.");

		Optional<Entity<Renting>> rentData = rents.get(rent);

		if (!rentData.isPresent()) {
			throw new IllegalArgumentException("Unknown rent.");
		}

		Renting renting = rentData.get().getElement();
		Rent theRent = renting.getRent();
		Contract.checkThat(theRent, r -> r.hasBegun(), "Rent has not begun.");
		
		Contract.checkThat(cancelled, m -> !m.containsKey(rent), "Rent has been cancelled.");
		Contract.checkThat(finished, m -> !m.containsKey(rent), "Rent is already finished.");
		
		if (theRent.isPast()) {
			rents.update(rent, new Renting(new ExtensionRent(theRent, ZonedDateTime.now()), renting.getUser(), renting.getRentable(), renting.getPrice()));
			exceeded.add(rent);
		}
	
		finished.put(rent, ZonedDateTime.now());
	}
	
	@Override
	public void extend(UUID rent, ZonedDateTime newEnd, long price) throws IllegalArgumentException {
		Objects.requireNonNull(rent, "Invalid rent.");

		Optional<Entity<Renting>> rentData = rents.get(rent);

		if (!rentData.isPresent()) {
			throw new IllegalArgumentException("Unknown rent.");
		}

		Renting renting = rentData.get().getElement();
		Rent theRent = renting.getRent();
		Contract.checkThat(theRent, r -> newEnd.isBefore(r.getEnd()), "New end date can't be before actual end date.");
		Contract.checkThat(theRent, r -> !r.isPast(), "Rent is finished.");
		
		Contract.checkThat(cancelled, m -> !m.containsKey(rent), "Rent has been cancelled.");
		Contract.checkThat(finished, m -> !m.containsKey(rent), "Rent is finished.");
		
		ExtensionRent extendedRent = new ExtensionRent(theRent, newEnd);
		checkAvailability(extendedRent, renting.getRentable());
		
		rents.update(rent,  new Renting(extendedRent, renting.getUser(), renting.getRentable(), price));
	}
	
	@Override
	public Optional<Entity<Renting>> getRent(UUID rent) {
		Objects.requireNonNull(rent, "Invalid rent.");
		
		return rents.get(rent);
	}

	@Override
	public Optional<UUID> getRentableForRent(UUID rent) {
		Objects.requireNonNull(rent, "Invalid rent.");
		
		Optional<Entity<Renting>> renting = rents.get(rent);
		
		if (!renting.isPresent()) {
			return Optional.<UUID>empty();
		}
		
		return Optional.<UUID>of(renting.get().getElement().getRentable());
	}
	
	@Override
	public List<Entity<Renting>> getRentablesByUser(UUID user) {
		return rents.entities()
			.filter(r -> r.getElement().getUser().equals(user))
			.collect(Collectors.toList());
	}
	
	private void checkAvailability(Rent rent, UUID rentable) {
	
		long horsinAround = rents.entities()
			.filter(r -> isRequestedRentable(r, rentable))
			.filter(r -> periodIsOverlaping(r, rent))
			.count();
	
		Contract.checkThat(horsinAround, n -> n == 0, "Rent period is unavailable.");
		
		boolean rentableIsAvailable = rents.entities()
			.filter(r -> isRequestedRentable(r, rentable))
			.filter(r -> isPast(r, rent))
			.filter(r -> !isFinished(r))
			.count() == 0;
		
		Contract.checkThat(rentableIsAvailable, r -> r, "Rentable is unavailable right now.");
	}
	
	private boolean isRequestedRentable(Entity<Renting> renting, UUID requested) {
		return renting.getElement().getRentable().equals(requested);
	}
	
	private boolean periodIsOverlaping(Entity<Renting> renting, Rent rent) {
		return !(rent.getStart().isAfter(renting.getElement().getRent().getEnd())
		|| rent.getEnd().isBefore(renting.getElement().getRent().getStart()));
	}

	private boolean isPast(Entity<Renting> renting, Rent rent) {
		return rent.getStart().isAfter(renting.getElement().getRent().getEnd());
	}
	
	private boolean isFinished(Entity<Renting> renting) {
		return finished.containsKey(renting.getId());
	}
}
