package fr.upem.rent;

import java.time.Duration;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

import fr.upem.contract.Contract;
import fr.upem.store.Storable;

public abstract class Rent implements Comparable<Rent>, Storable<Rent> {
	private static final long serialVersionUID = 7308229374523491310L;

	public enum Type {
		Regular,
		Extension
	}

	private final Type type;
	private final ZonedDateTime start;
	private final ZonedDateTime end;

	public Rent(ZonedDateTime start, ZonedDateTime end, Type type) {
		Objects.requireNonNull(start, "Invalid start date.");
		Objects.requireNonNull(end, "Invalid end date.");

		this.type = Objects.requireNonNull(type, "Invalid type.");
		this.start = Contract.checkThat(start, s -> s.isBefore(end), "Rent period is not correctly ordered.");
		this.end = end;
	}

	public Type getType() {
		return type;
	}

	public ZonedDateTime getStart() {
		return start;
	}

	public ZonedDateTime getEnd() {
		return end;
	}

	public Duration getDuration() {
		return Duration.between(start, end);
	}

	public boolean hasBegun() {
		return start.isAfter(ZonedDateTime.now());
	}

	public boolean isPast() {
		return end.isBefore(ZonedDateTime.now());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;

		result = prime * result + type.hashCode();
		result = prime * result + end.hashCode();
		result = prime * result + start.hashCode();

		return result;
	}

	@Override
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}

		if (!(o instanceof Rent)) {
			return false;
		}

		Rent rent = (Rent) o;

		return type == rent.type
		&& start.equals(rent.start)
		&& end.equals(rent.end);
	}

	@Override
	public String toString() {
		return "[" + start.format(DateTimeFormatter.RFC_1123_DATE_TIME) + " -> " + end.format(DateTimeFormatter.RFC_1123_DATE_TIME) + "]";
	}

	@Override
	public int compareTo(Rent rent) {
		return start.compareTo(rent.start);
	}
}
