package fr.upem.rent;

import java.time.ZonedDateTime;

public class RegularRent extends Rent {
	private static final long serialVersionUID = -7263994934535873417L;

	public RegularRent(ZonedDateTime start, ZonedDateTime end) {
		super(start, end, Type.Regular);
	}

	@Override
	public Rent copy() {
		return new RegularRent(this.getStart(), this.getEnd());
	}
}
