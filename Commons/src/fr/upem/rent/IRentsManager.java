package fr.upem.rent;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import fr.upem.rent.RentsManager.Renting;
import fr.upem.store.Entity;

public interface IRentsManager<T extends Enum<T>> {
	UUID add(Rent rent, UUID user, UUID rentable, long price) throws IllegalArgumentException;
	void cancel(UUID rent) throws IllegalArgumentException;
	void declareFinished(UUID rent) throws IllegalArgumentException;
	void extend(UUID rent, ZonedDateTime newEnd, long price) throws IllegalArgumentException;
	List<Entity<Renting>> getRentablesByUser(UUID user);
	Optional<UUID> getRentableForRent(UUID rent);
	Optional<Entity<Renting>> getRent(UUID rent);
}