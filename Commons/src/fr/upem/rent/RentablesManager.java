package fr.upem.rent;

import java.time.Duration;
import java.time.ZonedDateTime;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import fr.upem.contract.Contract;
import fr.upem.rate.Rate;
import fr.upem.store.ConcurrentStorage;
import fr.upem.store.Entity;
import fr.upem.store.Storage;

public class RentablesManager<T extends Enum<T>> implements RentingObserver<T>, IRentablesManager<T> {
	private static final long hoursSinceCreationForBuyableProperty = 17_532L;
	private static final int locationTimesForBuyableProperty = 1;
	
	private final Storage<Rentable<T>> rentables = new ConcurrentStorage<>();
	private final ConcurrentMap<UUID, Long> rentablesPrice = new ConcurrentHashMap<>();
	
	private final ConcurrentMap<UUID, ZonedDateTime> addingDates = new ConcurrentHashMap<>();
	private final ConcurrentMap<UUID, Integer> located = new ConcurrentHashMap<>();
	
	private final ConcurrentMap<UUID, List<UUID>> rentablesByOwner = new ConcurrentHashMap<>();
	
	private final Storage<Rate> rates = new ConcurrentStorage<>();
	private final ConcurrentMap<UUID, List<UUID>> rentablesRates = new ConcurrentHashMap<>();
	
	@Override
	public UUID add(Rentable<T> rentable, UUID owner, long price) {
		Objects.requireNonNull(rentable, "Invalid rentable.");
		Objects.requireNonNull(owner, "Invalid owner.");
		Contract.checkThat(price, p -> p >= 0, "Invalid price.");

		UUID id = rentables.insert(rentable);
		rentablesPrice.put(id, price);
		addingDates.put(id, ZonedDateTime.now());
		rentablesByOwner.compute(owner, (i, r) -> {
			if (r == null) {
				r = new LinkedList<>();
			}
			
			r.add(id);
			
			return r;
		});
		
		return id;
	}

	@Override
	public void modify(UUID rentable, Rentable<T> newRentable, long price) throws IllegalArgumentException {
		Objects.requireNonNull(rentable, "Invalid rentable.");
		Objects.requireNonNull(newRentable, "Invalid new rentable value.");
		Contract.checkThat(price, p -> p >= 0, "Invalid price.");

		if (rentables.update(rentable, newRentable) == 0) {
			throw new IllegalArgumentException("Unknown rentable.");
		}
		
		rentablesPrice.compute(rentable, (i, p) -> {
			return p != price ? price : p;
		});
	}

	@Override
	public void remove(UUID rentable) throws IllegalArgumentException {
		Objects.requireNonNull(rentable, "Invalid rentable.");
		
		// TODO : placer en file d'attente de suppression
		// Le rentable est supprimé lorsqu'il est rendu
		// dans le cas d'un rentable déjà loué lors de la suppression
		
		if (rentables.delete(rentable) == 0) {
			throw new IllegalArgumentException("Unknown rentable.");
		}
		
		rentablesRates.remove(rentable);
		located.remove(rentable);
		addingDates.remove(rentable);
		rentablesPrice.remove(rentable);
	}
	
	@Override
	public List<Entity<Rentable<T>>> getRentablesByOwner(UUID owner) throws IllegalArgumentException {
		List<UUID> userRentables = rentablesByOwner.get(owner);
		
		if (userRentables == null) {
			return new LinkedList<>();
		}
		
		return userRentables.stream().map(i -> rentables.get(i).get()).collect(Collectors.toList());
	}

	@Override
	public boolean isBuyable(UUID rentable) throws IllegalArgumentException {
		Objects.requireNonNull(rentable, "Invalid rentable.");
		
		ZonedDateTime adding = addingDates.get(rentable);
		
		if (adding == null) {
			throw new IllegalArgumentException("Unknown rentable.");
		}
		
		return checkIfBuyable(rentable);
	}

	@Override
	public List<Entity<Rentable<T>>> getBuyables() {
		return rentables.entities().filter(e -> checkIfBuyable(e.getId())).collect(Collectors.toList());
	}
	
	@Override
	public void comment(UUID rentable, Rate rate) throws IllegalArgumentException {
		Objects.requireNonNull(rentable, "Invalid rentable.");
		Objects.requireNonNull(rate, "Invalid rate.");

		if (!rentables.exists(rentable)) {
			throw new IllegalArgumentException("Unknown rentable.");
		}

		UUID rateID = rates.insert(rate);

		rentablesRates.compute(rentable, (r, c) -> {
			if (c == null) {
				c = new LinkedList<>();
			}

			c.add(rateID);

			return c;
		});
	}

	@Override
	public List<Entity<Rate>> getComments(UUID rentable) {
		Objects.requireNonNull(rentable, "Invalid rentable.");

		List<UUID> rates = rentablesRates.get(rentable);

		if (rates == null) {
			return new LinkedList<>();
		}

		return rates.stream()
			.map(r -> this.rates.get(r).get())
			.collect(Collectors.toList());
	}
	
	@Override
	public boolean isOwner(UUID owner, UUID rentable) {
		List<UUID> rentables = rentablesByOwner.get(owner);
		
		if (rentables == null) {
			return false;
		}
		
		return rentables.contains(rentable);
	}
	
	@Override
	public long getHourPrice(UUID rentable) throws IllegalArgumentException {
		Objects.requireNonNull(rentable, "Invalid rentable.");
		
		Long price = rentablesPrice.get(rentable);
		
		if (price == null) {
			throw new IllegalArgumentException("Unknown rentable.");
		}
		
		return price;
	}
	
	@Override
	public long getBuyingPrice(UUID rentable) throws IllegalArgumentException {
		Objects.requireNonNull(rentable, "Invalid rentable.");
		
		Contract.checkThat(rentable, r -> isBuyable(r), "Unknown buyable rentable.");
		
		return rentablesPrice.get(rentable) * 1000;
	}
	
	@Override
	public List<Entity<Rentable<T>>> getRentables() {
		return rentables.entities().collect(Collectors.toList());
	}

	@Override
	public List<Entity<Rentable<T>>> getFilteredRentables(Predicate<Entity<Rentable<T>>> rentableFilter, Predicate<Long> hourPriceFilter, Predicate<Long> buyingPriceFilter) {
		return rentables.entities()
			.filter(rentableFilter)
			.filter(r -> checkHourPrice(r, hourPriceFilter))
			.filter(r -> checkBuyingPrice(r, buyingPriceFilter))
			.collect(Collectors.toList());
	}
	
	/*
	 * Hack temporaire pour une simulation de louable ancien (supérieur à 2 an)
	 * /!\ Pas de vérification de présence, ni rien d'autre ! A VOS RISQUES ET PERILS. 
	 */
	public void changeAddingDate(UUID rentable, ZonedDateTime date) {
		addingDates.replace(rentable, date);
	}
	

	@Override
	public void rented(UUID rentable) {
		located.compute(rentable, (id, loc) -> {
			if (loc == null) {
				loc = new Integer(0);
			}
			
			return loc + 1;
		});
	}
	
	private boolean checkIfBuyable(UUID rentable) {
		ZonedDateTime adding = addingDates.get(rentable);
		int times = located.getOrDefault(rentable, 0);
		
		return checkAddingTime(adding) && checkLocatedTimes(times);
	}
	
	private boolean checkAddingTime(ZonedDateTime adding) {
		return Duration.between(adding, ZonedDateTime.now()).toHours() >= hoursSinceCreationForBuyableProperty;
	}
	
	private boolean checkLocatedTimes(int times) {
		return times >= locationTimesForBuyableProperty;
	}
	
	private boolean checkHourPrice(Entity<Rentable<T>> rentable, Predicate<Long> priceFilter) {
		return priceFilter.test(rentablesPrice.get(rentable.getId()));
	}
	
	private boolean checkBuyingPrice(Entity<Rentable<T>> rentable, Predicate<Long> priceFilter) {
		return priceFilter.test(rentablesPrice.get(rentable.getId()) * 1000);
	}
}
