package fr.upem.rent;

import java.util.List;
import java.util.UUID;
import java.util.function.Predicate;

import fr.upem.rate.Rate;
import fr.upem.store.Entity;

public interface IRentablesManager<T extends Enum<T>> {
	UUID add(Rentable<T> rentable, UUID owner, long price);
	void modify(UUID rentable, Rentable<T> newRentable, long price) throws IllegalArgumentException;
	void remove(UUID rentable) throws IllegalArgumentException;
	List<Entity<Rentable<T>>> getRentablesByOwner(UUID owner) throws IllegalArgumentException;
	boolean isBuyable(UUID rentable) throws IllegalArgumentException;
	List<Entity<Rentable<T>>> getBuyables();
	void comment(UUID rentable, Rate rate) throws IllegalArgumentException;
	List<Entity<Rate>> getComments(UUID rentable);
	boolean isOwner(UUID owner, UUID rentable);
	long getHourPrice(UUID rentable) throws IllegalArgumentException;
	long getBuyingPrice(UUID rentable) throws IllegalArgumentException;
	List<Entity<Rentable<T>>> getRentables();
	List<Entity<Rentable<T>>> getFilteredRentables(Predicate<Entity<Rentable<T>>> rentableFilter, Predicate<Long> hourPriceFilter, Predicate<Long> buyingPriceFilter);
}