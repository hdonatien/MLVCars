package fr.upem.net.http;

public enum HttpCode {
	Ok(200),
	BadRequest(400),
	Forbidden(403);
	
	private final int code;
	
	private HttpCode(int code) {
		this.code = code;
	}
	
	public int getCode() {
		return code;
	}
}
