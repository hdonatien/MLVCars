package fr.upem.store;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

public class Entity<T extends Serializable> implements Serializable {
	private static final long serialVersionUID = -1258377604668682135L;
	
	private final UUID id;
	private final T element;
	
	public Entity(UUID id, T element) {
		this.id = Objects.requireNonNull(id, "Invalid id.");
		this.element = Objects.requireNonNull(element, "Invalid element.");
	}

	public UUID getId() {
		return id;
	}
	
	public T getElement() {
		return element;
	}
}
