package fr.upem.store;

import java.io.Serializable;

public interface Storable<T> extends Serializable {
	public T copy();
}
