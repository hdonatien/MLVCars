package fr.upem.store;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.function.Predicate;
import java.util.stream.Stream;

public interface Storage<T extends Storable<T> & Serializable> extends Iterable<Entity<T>>, Fileable {
	UUID insert(T element);
	boolean exists(UUID id);
	Optional<Entity<T>> get(UUID id);
	List<Entity<T>> get(Predicate<T> criteria);
	int update(UUID id, T element);
	int update(Predicate<T> criteria, T element);
	int delete(UUID id);
	int delete(Predicate<T> criteria);
	Optional<T> remove(UUID id);
	Optional<T> removeFirst(Predicate<T> criteria);
	Stream<Entity<T>> entities();
}
