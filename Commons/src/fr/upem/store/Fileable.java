package fr.upem.store;

import java.io.IOException;
import java.nio.file.Path;

public interface Fileable {
	void saveToFile(Path path) throws IOException;
	void loadFromFile(Path path) throws IOException;
}
