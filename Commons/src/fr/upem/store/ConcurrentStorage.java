package fr.upem.store;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ConcurrentStorage<T extends Storable<T> & Serializable> implements Storage<T> {
	private final ConcurrentMap<UUID, T> rows = new ConcurrentHashMap<>();
	private final Object monitor = new Object();
	
	@Override
	public UUID insert(T element) {
		Objects.requireNonNull(element, "Invalid element.");
		
		synchronized (monitor) {
			UUID id = UUID.randomUUID();
			rows.put(id, element.copy());
			
			return id;
		}
	}
	
	@Override
	public boolean exists(UUID id) {
		Objects.requireNonNull(id, "Invalid id.");
		
		return rows.containsKey(id);
	}
	
	@Override
	public Optional<Entity<T>> get(UUID id) {
		Objects.requireNonNull(id, "Invalid id.");
		
		T element = rows.get(id);
		
		if (element != null) {
			element = element.copy();
		}
		
		return element == null ? Optional.<Entity<T>>empty()
			: Optional.<Entity<T>>of(new Entity<T>(id, element));
	}
	
	@Override
	public List<Entity<T>> get(Predicate<T> criteria) {
		Objects.requireNonNull(criteria, "Invalid criteria.");
		
		return rows.entrySet()
			.stream()
			.filter(row -> criteria.test(row.getValue()))
			.map(row -> new Entity<T>(row.getKey(), row.getValue().copy()))
			.collect(Collectors.toList());
	}
	
	@Override
	public int update(UUID id, T element) {
		Objects.requireNonNull(id, "Invalid id.");
		Objects.requireNonNull(element, "Invalid element.");
		
		return rows.replace(id, element.copy()) == null ? 0 : 1;
	}
	
	@Override
	public int update(Predicate<T> criteria, T element) {
		Objects.requireNonNull(criteria, "Invalid criteria.");
		Objects.requireNonNull(element, "Invalid element.");
		
		int affected = 0;
		
		synchronized (monitor) {
			for (Map.Entry<UUID, T> row : rows.entrySet()) {
				if (criteria.test(row.getValue())) {
					rows.replace(row.getKey(), element.copy());
					affected++;
				}
			}
		}
		
		return affected;
	}
	
	@Override
	public int delete(UUID id) {
		Objects.requireNonNull(id, "Invalid id.");
		
		synchronized (monitor) {
			return rows.remove(id) == null ? 0 : 1;
		}
	}
	
	@Override
	public int delete(Predicate<T> criteria) {
		Objects.requireNonNull(criteria, "Invalid criteria.");
		
		synchronized (monitor) {
			int previous = rows.size();
			
			rows.values().removeIf(criteria);
			
			return previous - rows.size();
		}
	}
	
	@Override
	public Optional<T> remove(UUID id) {
		Objects.requireNonNull(id, "Invalid id.");
		
		synchronized (monitor) {
			return Optional.<T>ofNullable(rows.remove(id));
		}
	}
	
	@Override
	public Optional<T> removeFirst(Predicate<T> criteria) {
		Objects.requireNonNull(criteria, "Invalid criteria.");
		
		T element = null;
		
		synchronized (monitor) {
			for (T elem : rows.values()) {
				if (criteria.test(elem)) {
					element = elem;
					rows.values().remove(elem);
					
					break;
				}
			}
			
			return Optional.<T>ofNullable(element);
		}
	}

	@Override
	public Iterator<Entity<T>> iterator() {
		return entities().iterator();
	}

	@Override
	public Stream<Entity<T>> entities() {
		return rows.entrySet().stream().map(r -> new Entity<T>(r.getKey(), r.getValue().copy()));
	}
	
	@Override
	public void saveToFile(Path path) throws IOException {
		Objects.requireNonNull(path, "Invalid file.");
		
		try (
			OutputStream file = new FileOutputStream(path.toFile());
			OutputStream buffer = new BufferedOutputStream(file);
			ObjectOutput output = new ObjectOutputStream(buffer);
		) {
			output.writeLong(rows.size());
			
			for (Map.Entry<UUID, T> row : rows.entrySet()) {
				output.writeObject(new Entity<T>(row.getKey(), row.getValue()));
			}
		}
	}

	@Override
	public void loadFromFile(Path path) throws IOException {
		Objects.requireNonNull(path, "Invalid file.");
		
		Map<UUID, T> loadedRows = new HashMap<>();
		
		try(
			InputStream file = new FileInputStream("quarks.ser");
			InputStream buffer = new BufferedInputStream(file);
			ObjectInput input = new ObjectInputStream(buffer);
		) {
			long size = input.readLong();
			
			while (size > 0) {
				@SuppressWarnings("unchecked")
				Entity<T> entity = (Entity<T>) input.readObject();
				
				loadedRows.put(entity.getId(), entity.getElement());
				
				size--;
			}
			
			synchronized (monitor) {
				rows.clear();
				rows.putAll(loadedRows);
			}
		} catch(ClassNotFoundException ex){
			throw new IOException("Given file doesn't represent the same type of storage.");
		}
	}
}
