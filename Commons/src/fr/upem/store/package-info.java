/**
 * Contains some classes for data storage, like uniquely identifiable entities
 * ({@link Entity}), or identifier-value association tables ({@link Storage}).
 */
package fr.upem.store;