package fr.upem.user;

import java.io.Serializable;
import java.util.UUID;

public class AuthenticationTicket implements Serializable {
    private static final long serialVersionUID = 1410926965182826683L;

    private final UUID token;

    public AuthenticationTicket() {
        this.token = UUID.randomUUID();
    }
    
    public AuthenticationTicket(String id) throws IllegalArgumentException {
    	this.token = UUID.fromString(id);
    }

    public UUID getToken() {
        return token;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (!(o instanceof AuthenticationTicket)) {
            return false;
        }

        AuthenticationTicket ticket = (AuthenticationTicket) o;

        return token.equals(ticket.token);
    }

    @Override
    public int hashCode() {
        return token.hashCode();
    }

    @Override
    public String toString() {
    	return "Ticket: " + token.toString();
    }
}
