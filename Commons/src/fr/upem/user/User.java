package fr.upem.user;

import java.io.Serializable;
import java.util.Objects;

import fr.upem.store.Storable;

public class User<T extends Enum<T>> implements Storable<User<T>>, Serializable {
	public static enum Title {
		Mr,
		Mrs
	}

	private static final long serialVersionUID = -8851163614665081825L;

	private final Title title;
	private final String firstName;
	private final String lastName;
	private final T type;

	public User(Title title, String firstName, String lastName, T type) {
		this.title = Objects.requireNonNull(title, "Invalid title.");
		this.firstName = Objects.requireNonNull(firstName, "Invalid first name.");
		this.lastName = Objects.requireNonNull(lastName, "Invalid last name.");
		this.type = Objects.requireNonNull(type, "Invalid type.");
	}

	public Title getTitle() {
		return title;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}
	
	public T getType() {
		return type;
	}

	@Override
	public User<T> copy() {
		return new User<T>(title, firstName, lastName, type);
	}
}
