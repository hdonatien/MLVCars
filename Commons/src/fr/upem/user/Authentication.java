package fr.upem.user;

import java.io.Serializable;
import java.util.Objects;

public class Authentication implements Serializable {
	private static final long serialVersionUID = -2703555147139075179L;
	
	private final String login;
	private final String password;
	
	public Authentication(String login, String password) {
		this.login = Objects.requireNonNull(login, "Invalid login.");
		this.password = Objects.requireNonNull(password, "Invalid password.");
	}
	
	public boolean check(String login, String password) {
		return login.equals(this.login) && password.equals(this.password);
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		
		if (!(o instanceof Authentication)) {
			return false;
		}
		
		Authentication auth = (Authentication) o;
		
		return login.equals(auth.login) && password.equals(auth.password);
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		
		result = prime * result + login.hashCode();
		result = prime * result + password.hashCode();
		
		return result;
	}
}