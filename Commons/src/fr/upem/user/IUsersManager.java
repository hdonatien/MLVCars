package fr.upem.user;

import java.util.Optional;
import java.util.UUID;

import fr.upem.store.Entity;

public interface IUsersManager<T extends Enum<T>> {
	Optional<UUID> checkSecurity(AuthenticationTicket ticket);
	UUID add(User<T> user, Authentication authentication, Long bankAccount);
	Optional<AuthenticationTicket> connect(Authentication authentication);
	void disconnect(AuthenticationTicket ticket);
	Optional<Entity<User<T>>> getAuthenticatedUser(AuthenticationTicket ticket);
	Optional<Long> getUserBankAccount(UUID user) throws IllegalArgumentException;
}
