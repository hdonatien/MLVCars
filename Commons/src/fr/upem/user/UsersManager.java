package fr.upem.user;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.nio.file.Path;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import fr.upem.store.ConcurrentStorage;
import fr.upem.store.Entity;
import fr.upem.store.Fileable;
import fr.upem.store.Storage;

public class UsersManager<T extends Enum<T>> implements IUsersManager<T>, Fileable {
	private class UserData implements Serializable {
		private static final long serialVersionUID = 1725193585985592854L;

		final UUID user;
		final long lastAction;

		UserData(UUID user) {
			this.user = Objects.requireNonNull(user, "Invalid user.");
			this.lastAction = System.currentTimeMillis();
		}

		boolean userIsInactive(long currentTime, long limit) {
			return currentTime - lastAction > limit * 1000;
		}
	}

	private final Storage<User<T>> users = new ConcurrentStorage<>();
	private final ConcurrentMap<UUID, Long> usersBankAccount = new ConcurrentHashMap<>();

	private final ConcurrentMap<Authentication, UUID> usersAuthData = new ConcurrentHashMap<>();
	private final ConcurrentMap<AuthenticationTicket, UserData> tickets = new ConcurrentHashMap<>();

	public UsersManager(long inactivityLimit) {
		if (inactivityLimit > 0) {
			new Thread(() -> {
				try {
					while (true) {
						Thread.sleep(60_000);
	
						long current = System.currentTimeMillis();
	
						tickets.entrySet().removeIf(t -> t.getValue().userIsInactive(current, inactivityLimit));
					}
				} catch (InterruptedException e) {
					// Do nothing
				}
			}).start();
		}
	}

	@Override
	public Optional<UUID> checkSecurity(AuthenticationTicket ticket) {
		Objects.requireNonNull(ticket, "Invalid authentication ticket.");

		UserData data = tickets.get(ticket);

		if (data == null) {
			return Optional.empty();
		}

		return Optional.of(data.user);
	}

	@Override
	public UUID add(User<T> user, Authentication authentication, Long bankAccount) {
		Objects.requireNonNull(user, "Invalid user.");

		UUID id = users.insert(user);
		usersAuthData.put(authentication, id);
		usersBankAccount.put(id, bankAccount);

		return id;
	}

	@Override
	public Optional<AuthenticationTicket> connect(Authentication authentication) {
		Objects.requireNonNull(authentication, "Invalid authentication data.");

		UUID user = usersAuthData.get(authentication);
		AuthenticationTicket ticket = null;

		if (user != null) {
			ticket = new AuthenticationTicket();
			tickets.put(ticket, new UserData(user));
		}

		return Optional.<AuthenticationTicket>ofNullable(ticket);
	}

	@Override
	public void disconnect(AuthenticationTicket ticket) {
		tickets.remove(ticket);
	}

	@Override
	public Optional<Entity<User<T>>> getAuthenticatedUser(AuthenticationTicket ticket) {
		Optional<UUID> user = this.checkSecurity(ticket);

		if (!user.isPresent()) {
			return Optional.<Entity<User<T>>>empty();
		}
		
		return users.get(user.get());
	}

	@Override
	public Optional<Long> getUserBankAccount(UUID user) throws IllegalArgumentException {
		Objects.requireNonNull(user, "Invalid user.");
		
		return Optional.<Long>ofNullable(usersBankAccount.get(user));
	}

	@Override
	public void saveToFile(Path path) throws IOException {
		Objects.requireNonNull(path, "Invalid file.");
		
//		private final ConcurrentMap<UUID, Long> usersBankAccount = new ConcurrentHashMap<>();
//		private final ConcurrentMap<Authentication, UUID> usersAuthData = new ConcurrentHashMap<>();
//		private final ConcurrentMap<AuthenticationTicket, UserData> tickets = new ConcurrentHashMap<>();
		
		users.saveToFile(path);
		
		try (
			OutputStream file = new FileOutputStream(path.toFile());
			OutputStream buffer = new BufferedOutputStream(file);
			ObjectOutput output = new ObjectOutputStream(buffer);
		) {
			output.writeLong(usersBankAccount.size());
			
			for (Map.Entry<UUID, Long> account : usersBankAccount.entrySet()) {
				output.writeObject(account.getKey());
				output.writeLong(account.getValue());
			}
			
			output.writeLong(usersAuthData.size());
			
			for (Map.Entry<Authentication, UUID> auth : usersAuthData.entrySet()) {
				output.writeObject(auth.getKey());
				output.writeObject(auth.getValue());
			}
			
//			output.writeLong(tickets.size());
//			
//			for (Map.Entry<AuthenticationTicket, UserData> ticket : tickets.entrySet()) {
//				output.writeObject(ticket.getKey());
//				output.writeObject(ticket.getValue());
//			}
		}
	}

	@Override
	public void loadFromFile(Path path) throws IOException {
		Objects.requireNonNull(path, "Invalid file.");
		
		
	}
}
