package fr.upem.contract;

import java.util.function.Predicate;

/**
 * Some tools to check classes' contracts.
 */
public class Contract {
	/**
	 * Constraint checker for an element.<br>
	 * If the constraint is not satisfied, throws an error.
	 * 
	 * @param <T> Type of the element to check.
	 * @param elem Element submitted to the constraint.
	 * @param check Constraint to check.
	 * @param message Error message in case of not satisfied constraint.
	 * @return Element submitted to the constraint.
	 * @throws IllegalArgumentException Error message in case of not satisfied constraint.
	 */
	public static <T> T checkThat(T elem, Predicate<T> check, String message) throws IllegalArgumentException {
		if (!check.test(elem)) {
			throw new IllegalArgumentException(message);
		}
		
		return elem;
	}
}
