package fr.upem.rate;

import java.util.Objects;

import fr.upem.store.Storable;

public class Rate implements Comparable<Rate>, Storable<Rate> {
	private static final long serialVersionUID = -3807035844909292018L;
	
	private final String comment;
	private final int mark;
	
	public Rate(String comment, int mark) throws IllegalArgumentException {
		if (mark < 0 || mark > 10) {
			throw new IllegalArgumentException("Invalid mark.");
		}
		
		this.comment = Objects.requireNonNull(comment, "Invalid comment.");
		this.mark = mark;
	}
	
	public String getComment() {
		return comment;
	}
	
	public int getMark() {
		return mark;
	}

	@Override
	public int compareTo(Rate rate) {
		// Represents the relevance of a comment compared to another.
		// A much longer comment is generally more relevant.
		// In our case, a difference of less than 30% is not considered significant.
		int difference = (int) ((float) comment.length() / rate.comment.length() * 100);
		
		if (mark != rate.mark) {
			return mark - rate.mark;
		}
		
		return difference < 30 ? 0 : (difference < 100 ? -1 : 1);
	}

	@Override
	public Rate copy() {
		return new Rate(comment, mark);
	}
}
