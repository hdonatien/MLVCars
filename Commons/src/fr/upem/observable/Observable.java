package fr.upem.observable;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.function.Consumer;

public class Observable<T> {
	private final List<T> observers = new LinkedList<>();

	public void register(T observer) throws IllegalArgumentException {
		Objects.requireNonNull(observer, "Invalid observer.");

		if (observers.contains(observer)) {
			throw new IllegalArgumentException("Already known observer.");
		}

		observers.add(observer);
	}

	public void unregister(T observer) throws IllegalArgumentException {
		Objects.requireNonNull(observer, "Invalid observer.");

		if (observers.remove(observer)) {
			throw new IllegalArgumentException("Unknown observer.");
		}
	}
	
	protected void notify(Consumer<T> message) {
		observers.forEach(message::accept);
	}
}
