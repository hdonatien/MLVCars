package fr.upem.output;

public class ColoredOutput {
	public static enum OutputColor {
		Reset("\u001B[0m"),
		Black("\u001B[30m"),
		Red("\u001B[31m"),
		Green("\u001B[32m"),
		Yellow("\u001B[33m"),
		Blue("\u001B[34m"),
		Purple("\u001B[35m"),
		Cyan("\u001B[36m"),
		White("\u001B[37m");
		
		private final String code;
		
		private OutputColor(String code) {
			this.code = code;
		}
		
		public String getCode() {
			return code;
		}
	}
	
	public static void println() {
		println("");
	}
	
	public static void println(String text) {
		print(text + "\n");
	}
	
	public static void print(String text) {
		System.out.print(text);
	}
	
	public static void println(String text, OutputColor color) {
		print(text + "\n", color);
	}
	
	public static void print(String text, OutputColor color) {
		System.out.print(color.getCode() + text + OutputColor.Reset.code);
	}
}
