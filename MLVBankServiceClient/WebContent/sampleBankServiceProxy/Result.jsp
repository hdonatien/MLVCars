<%@page contentType="text/html;charset=UTF-8"%>
<% request.setCharacterEncoding("UTF-8"); %>
<HTML>
<HEAD>
<TITLE>Result</TITLE>
</HEAD>
<BODY>
<H1>Result</H1>

<jsp:useBean id="sampleBankServiceProxyid" scope="session" class="fr.upem.mlvbank.BankServiceProxy" />
<%
if (request.getParameter("endpoint") != null && request.getParameter("endpoint").length() > 0)
sampleBankServiceProxyid.setEndpoint(request.getParameter("endpoint"));
%>

<%
String method = request.getParameter("method");
int methodID = 0;
if (method == null) methodID = -1;

if(methodID != -1) methodID = Integer.parseInt(method);
boolean gotMethod = false;

try {
switch (methodID){ 
case 2:
        gotMethod = true;
        java.lang.String getEndpoint2mtemp = sampleBankServiceProxyid.getEndpoint();
if(getEndpoint2mtemp == null){
%>
<%=getEndpoint2mtemp %>
<%
}else{
        String tempResultreturnp3 = org.eclipse.jst.ws.util.JspUtils.markup(String.valueOf(getEndpoint2mtemp));
        %>
        <%= tempResultreturnp3 %>
        <%
}
break;
case 5:
        gotMethod = true;
        String endpoint_0id=  request.getParameter("endpoint8");
            java.lang.String endpoint_0idTemp = null;
        if(!endpoint_0id.equals("")){
         endpoint_0idTemp  = endpoint_0id;
        }
        sampleBankServiceProxyid.setEndpoint(endpoint_0idTemp);
break;
case 10:
        gotMethod = true;
        fr.upem.mlvbank.BankService getBankService10mtemp = sampleBankServiceProxyid.getBankService();
if(getBankService10mtemp == null){
%>
<%=getBankService10mtemp %>
<%
}else{
        if(getBankService10mtemp!= null){
        String tempreturnp11 = getBankService10mtemp.toString();
        %>
        <%=tempreturnp11%>
        <%
        }}
break;
case 13:
        gotMethod = true;
        String currency_2id=  request.getParameter("currency18");
            java.lang.String currency_2idTemp = null;
        if(!currency_2id.equals("")){
         currency_2idTemp  = currency_2id;
        }
        String amount_3id=  request.getParameter("amount20");
        long amount_3idTemp  = Long.parseLong(amount_3id);
        String type_4id=  request.getParameter("type22");
        int type_4idTemp  = Integer.parseInt(type_4id);
        String accountNumber_5id=  request.getParameter("accountNumber24");
        long accountNumber_5idTemp  = Long.parseLong(accountNumber_5id);
        %>
        <jsp:useBean id="fr1upem1mlvbank1Transaction_1id" scope="session" class="fr.upem.mlvbank.Transaction" />
        <%
        fr1upem1mlvbank1Transaction_1id.setCurrency(currency_2idTemp);
        fr1upem1mlvbank1Transaction_1id.setAmount(amount_3idTemp);
        fr1upem1mlvbank1Transaction_1id.setType(type_4idTemp);
        fr1upem1mlvbank1Transaction_1id.setAccountNumber(accountNumber_5idTemp);
        int transact13mtemp = sampleBankServiceProxyid.transact(fr1upem1mlvbank1Transaction_1id);
        String tempResultreturnp14 = org.eclipse.jst.ws.util.JspUtils.markup(String.valueOf(transact13mtemp));
        %>
        <%= tempResultreturnp14 %>
        <%
break;
case 26:
        gotMethod = true;
        String number_6id=  request.getParameter("number35");
        long number_6idTemp  = Long.parseLong(number_6id);
        fr.upem.mlvbank.BankAccountSnapShot getSnapShot26mtemp = sampleBankServiceProxyid.getSnapShot(number_6idTemp);
if(getSnapShot26mtemp == null){
%>
<%=getSnapShot26mtemp %>
<%
}else{
%>
<TABLE>
<TR>
<TD COLSPAN="3" ALIGN="LEFT">returnp:</TD>
<TR>
<TD WIDTH="5%"></TD>
<TD COLSPAN="2" ALIGN="LEFT">currency:</TD>
<TD>
<%
if(getSnapShot26mtemp != null){
java.lang.String typecurrency29 = getSnapShot26mtemp.getCurrency();
        String tempResultcurrency29 = org.eclipse.jst.ws.util.JspUtils.markup(String.valueOf(typecurrency29));
        %>
        <%= tempResultcurrency29 %>
        <%
}%>
</TD>
<TR>
<TD WIDTH="5%"></TD>
<TD COLSPAN="2" ALIGN="LEFT">number:</TD>
<TD>
<%
if(getSnapShot26mtemp != null){
%>
<%=getSnapShot26mtemp.getNumber()
%><%}%>
</TD>
<TR>
<TD WIDTH="5%"></TD>
<TD COLSPAN="2" ALIGN="LEFT">balance:</TD>
<TD>
<%
if(getSnapShot26mtemp != null){
%>
<%=getSnapShot26mtemp.getBalance()
%><%}%>
</TD>
</TABLE>
<%
}
break;
case 37:
        gotMethod = true;
        String currencyFrom_7id=  request.getParameter("currencyFrom40");
            java.lang.String currencyFrom_7idTemp = null;
        if(!currencyFrom_7id.equals("")){
         currencyFrom_7idTemp  = currencyFrom_7id;
        }
        String currencyTo_8id=  request.getParameter("currencyTo42");
            java.lang.String currencyTo_8idTemp = null;
        if(!currencyTo_8id.equals("")){
         currencyTo_8idTemp  = currencyTo_8id;
        }
        String amount_9id=  request.getParameter("amount44");
        long amount_9idTemp  = Long.parseLong(amount_9id);
        long convert37mtemp = sampleBankServiceProxyid.convert(currencyFrom_7idTemp,currencyTo_8idTemp,amount_9idTemp);
        String tempResultreturnp38 = org.eclipse.jst.ws.util.JspUtils.markup(String.valueOf(convert37mtemp));
        %>
        <%= tempResultreturnp38 %>
        <%
break;
}
} catch (Exception e) { 
%>
Exception: <%= org.eclipse.jst.ws.util.JspUtils.markup(e.toString()) %>
Message: <%= org.eclipse.jst.ws.util.JspUtils.markup(e.getMessage()) %>
<%
return;
}
if(!gotMethod){
%>
result: N/A
<%
}
%>
</BODY>
</HTML>